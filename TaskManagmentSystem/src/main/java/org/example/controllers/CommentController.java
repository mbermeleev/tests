package org.example.controllers;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.DTOs.CommentDTO;
import org.example.services.interfaces.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name="Контроллер для работы с комментариями",
        description="Endpoint`ы связанные с моделью комментариев(Comment)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/comment")
public class CommentController {

    private final CommentService commentService;

    @Operation(
            summary = "Для нового комментария",
            description = "Позволяет добавить новый комментарий")
    @PostMapping("/addNewComment")
    public ResponseEntity<CommentDTO> addNewComment(
            @Parameter(description = "Сущность комментария с условного \"фронта\"", required = true)
            @RequestBody CommentDTO commentDTO) {

        return ResponseEntity.ok(commentService.addNewComment(commentDTO));
    }

    @Operation(
            summary = "Для получения комментариев",
            description = "Позволяет получить комментарии следующие за последним отображаемым")
    @GetMapping("/getNextComments/{task_id}/{last_comment_id}")
    public ResponseEntity<List<CommentDTO>> getNextComments(
            @Parameter(description = "URL параметр, передаем id задачи", required = true)
            @PathVariable Long task_id,
            @Parameter(description = "URL параметр передаем id последнего отображаемого комментария", required = true)
            @PathVariable Long last_comment_id) {

        return ResponseEntity.ok(commentService.getNextComments(task_id, last_comment_id));
    }
}
