package org.example.controllers;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.DTOs.*;
import org.example.services.interfaces.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name="Контроллер пользователя",
        description="Endpoint`ы связанные с моделью пользователя(регистрация/аутентификация и т.п.)")
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @Operation(
            summary = "Аутентификация пользователя",
            description = "Позволяет аутентифицировать пользователя и получить его токен")
    @PostMapping("/auth")
    public ResponseEntity<String> accountAuthenticate(
            @RequestBody @Parameter(description = "Сущность аутентификации") AccountAuthenticationDTO accountAuthenticationDTO) {
        return ResponseEntity.ok()
                .body(accountService.accountAuthenticate(accountAuthenticationDTO));
    }

    @Operation(
            summary = "Регистрация пользователя",
            description = "Позволяет зарегистрировать пользователя"
    )
    @PostMapping("/signUp")
    public ResponseEntity<String> signUp(@RequestBody @Parameter(description = "Сущность регистрации")
                                         SignUpDto signUpDto) {
        return ResponseEntity.ok().body(accountService.signUp(signUpDto));
    }
}
