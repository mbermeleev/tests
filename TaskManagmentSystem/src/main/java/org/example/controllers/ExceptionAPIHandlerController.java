package org.example.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.enums.StatusForRequest;
import org.example.exceptions.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Контроллер обработки исключений",
        description = "Позволяет обработать исключения")
@RestControllerAdvice
public class ExceptionAPIHandlerController {

    @Hidden
    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<AccountNotFoundException> personNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AccountNotFoundException());
    }

    @Hidden
    @ExceptionHandler(TaskNotFoundException.class)
    public ResponseEntity<TaskNotFoundException> taskNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new TaskNotFoundException());
    }

    @Hidden
    @ExceptionHandler(NotEnoughRightsException.class)
    public ResponseEntity<NotEnoughRightsException> notEnoughRightsException() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new NotEnoughRightsException());
    }

    @Hidden
    @ExceptionHandler(AccountAlreadySignedUpException.class)
    public ResponseEntity<AccountAlreadySignedUpException> accountAlreadySignedUpException() {
        return ResponseEntity.status(StatusForRequest.ALREADY_REGISTERED.getStatusCode())
                .body(new AccountAlreadySignedUpException());
    }

    @Hidden
    @ExceptionHandler(NotValidEmailException.class)
    public ResponseEntity<NotValidEmailException> notValidEmailException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(new NotValidEmailException());
    }
}

