package org.example.controllers;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.DTOs.TaskDTO;
import org.example.exceptions.NotEnoughRightsException;
import org.example.services.interfaces.TaskService;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name="Контроллер для работы с задачами",
        description="Endpoint`ы связанные с моделью задач(Task)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;

    @Operation(
            summary = "Для создания",
            description = "Позволяет создать новую задачу")
    @PostMapping("/addNewTask")
    public ResponseEntity<HttpStatus> addNewTask(
            @Parameter(description = "Сущность задачи с условного \"фронта\"", required = true)
            @RequestBody TaskDTO taskDTO) {
        return ResponseEntity.ok(taskService.addNewTask(taskDTO));
    }

    @Operation(
            summary = "Для редактирования",
            description = "Позволяет отредактировать уже существующую задачу, может выполнить только создатель")
    @PatchMapping("/editTask")
    public ResponseEntity<TaskDTO> editTask(
            @Parameter(description = "Сущность задачи с условного \"фронта\"", required = true)
            @RequestBody TaskDTO taskDTO,
            @Parameter(description = "Заголовки запроса", required = true)
            @RequestHeader HttpHeaders headers) {
        return ResponseEntity.ok(taskService.editTask(taskDTO,
                headers.toSingleValueMap().get("authorization").substring(7)));
    }

    @Operation(
            summary = "Для удаления",
            description = "Позволяет удалить задачу, если ты являешься ее создателем")
    @DeleteMapping("/deleteTask/{task_id}")
    public ResponseEntity<String> deleteTask(
            @Parameter(description = "Id задачи", required = true)
            @PathVariable Long task_id,
            @Parameter(description = "Заголовки запроса", required = true)
            @RequestHeader HttpHeaders headers) {
        try {
            return ResponseEntity.ok(taskService.deleteTask(task_id,
                    headers.toSingleValueMap().get("authorization").substring(7)));
        } catch (NotEnoughRightsException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }

    @Operation(
            summary = "Для получения",
            description = "Позволяет получить определенную задачу по ее id")
    @GetMapping("/getTask/{task_id}")
    public ResponseEntity<TaskDTO> getTask(
            @Parameter(description = "Id задачи", required = true)
            @PathVariable Long task_id) {
        return ResponseEntity.ok(taskService.getTask(task_id));
    }

    @Operation(
            summary = "Для получения",
            description = "Позволяет получить группу задач по исполнителю")
    @GetMapping("/getTasksByExecutor/{executor_id}/{task_id_after_which}")
    public ResponseEntity<List<TaskDTO>> getTasksByExecutor(
            @Parameter(description = "Id исполнителя", required = true)
            @PathVariable Long executor_id,
            @Parameter(description = "Id задачи после которой будут получены другие задачи", required = true)
            @PathVariable Long task_id_after_which) {
        return ResponseEntity.ok(taskService.getTasksByExecutor(executor_id, task_id_after_which));
    }

    @Operation(
            summary = "Для получения",
            description = "Позволяет получить группу задач по создателю")
    @GetMapping("/getTasksByAuthor/{author_id}/{task_id_after_which}")
    public ResponseEntity<List<TaskDTO>> getTasksByAuthor(
            @Parameter(description = "Id создателя", required = true)
            @PathVariable Long author_id,
            @Parameter(description = "Id задачи после которой будут получены другие задачи", required = true)
            @PathVariable Long task_id_after_which) {
        return ResponseEntity.ok(taskService.getTasksByAuthor(author_id, task_id_after_which));
    }

    @Operation(
            summary = "Для редактирования",
            description = "Позволяет изменить статус задачи, может выполнить только исполнитель/создатель")
    @PatchMapping("/changeStatus/{task_id}/{status_code}")
    public ResponseEntity<String> changeStatus(
            @Parameter(description = "Id задачи", required = true)
            @PathVariable Long task_id,
            @Parameter(description = "Интовое значение статуса задачи", required = true)
            @PathVariable Integer status_code,
            @Parameter(description = "Заголовки запроса", required = true)
            @RequestHeader HttpHeaders headers) {
        return ResponseEntity.ok(taskService.changeStatus(task_id, status_code,
                headers.toSingleValueMap().get("authorization").substring(7)));
    }

    @Operation(
            summary = "Для редактирования",
            description = "Позволяет сменить исполнителя задачи, может выполнить только создатель")
    @PatchMapping("/changeExecutor/{task_id}/{executor_id}")
    public ResponseEntity<String> changeExecutor(
            @Parameter(description = "Id задачи", required = true)
            @PathVariable Long task_id,
            @Parameter(description = "Id нового исполнителя", required = true)
            @PathVariable Long executor_id,
            @Parameter(description = "Заголовки запроса", required = true)
            @RequestHeader HttpHeaders headers) {

        return ResponseEntity.ok(taskService.changeExecutor(task_id, executor_id,
                headers.toSingleValueMap().get("authorization").substring(7)));
    }
}
