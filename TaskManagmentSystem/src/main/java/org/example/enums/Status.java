package org.example.enums;

public enum Status {

    NONE(0),
    CREATED(1),
    IN_PROGRESS(2),
    PENDING(3),
    COMPLETED(4);

    private final int statusCode;

    Status(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
