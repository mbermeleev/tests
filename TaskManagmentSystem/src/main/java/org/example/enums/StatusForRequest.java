package org.example.enums;

public enum StatusForRequest {

    ALREADY_REGISTERED(600);

    private final int statusCode;

    StatusForRequest(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
