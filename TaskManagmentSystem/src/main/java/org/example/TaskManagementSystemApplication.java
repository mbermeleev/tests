package org.example;

import lombok.RequiredArgsConstructor;
import org.example.enums.Priority;
import org.example.enums.Role;
import org.example.enums.Status;
import org.example.models.Account;
import org.example.models.Comment;
import org.example.models.Task;
import org.example.repositories.AccountRepository;
import org.example.repositories.CommentRepository;
import org.example.repositories.TaskRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@SpringBootApplication
@RequiredArgsConstructor
public class TaskManagementSystemApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TaskManagementSystemApplication.class, args);
    }

    private final AccountRepository accountRepository;
    private final TaskRepository taskRepository;
    private final CommentRepository commentRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void run(String... args) {

        for (int i = 1; i <= 4; i++) {
            accountRepository.save(Account.builder()
                    .firstName("account" + i)
                    .lastName("account" + i)
                    .email("email" + i + "@mail.ru")
                    .password(passwordEncoder.encode("password" + i))
                    .role(i == 1 ? Role.ADMIN : Role.USER)
                    .build());
        }

        Account account1 = accountRepository.findById(1L).get();
        Account account2 = accountRepository.findById(2L).get();
        Account account3 = accountRepository.findById(3L).get();

        for (int i = 1; i <= 10; i++) {
            taskRepository.save(Task.builder()
                    .title("Task" + i)
                    .description("description" + i)
                    .status(i % 2 == 0 ? Status.CREATED : Status.IN_PROGRESS)
                    .priority(i % 2 == 0 ? Priority.HIGH : Priority.LOW)
                    .createDate(Instant.now().plusSeconds(i * 60))
                    .author(account1)
                    .executor(i % 3 == 0 ? account2 : account3)
                    .build());
        }

        Task task1 = taskRepository.findById(1L).get();
        Task task2 = taskRepository.findById(1L).get();

        for (int i = 1; i <= 10; i++) {
            commentRepository.save(Comment.builder()
                            .text("Comment" + i)
                            .createDate(Instant.now().plusSeconds(i * 60))
                            .author(i % 2 == 0 ? account1 : account2)
                            .task(i % 2 == 0 ? task1 : task2)
                            .build());
        }
    }
}
