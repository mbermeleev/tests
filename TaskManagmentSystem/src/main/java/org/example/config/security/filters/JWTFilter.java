package org.example.config.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import org.example.config.security.details.CustomUserDetails;
import org.example.enums.Role;
import org.example.models.Account;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.Instant;

import static org.example.utils.StringUtils.*;
import static org.example.utils.SecurityUtils.*;

@Component
public class JWTFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
            String authHeader = request.getHeader(AUTHORIZATION);
            if (authHeader != null && authHeader.startsWith(BEARER)) {
                String token = authHeader.substring(BEARER.length());
                if (token.isBlank()) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, INVALID_JWT_TOKEN_IN_BEARER);
                } else {
                    DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SECRET_KEY))
                            .build()
                            .verify(token);

                    var dateFromToken = decodedJWT.getIssuedAt();

                    if (dateFromToken.toInstant().isAfter(Instant.now().plusSeconds(3600)))
                        response.sendError(HttpServletResponse.SC_FORBIDDEN, TOKEN_EXPIRED);
                    else {
                        Account account = Account.builder()
                                .id(decodedJWT.getClaim(ID_CLAIM).asLong())
                                .firstName(decodedJWT.getClaim(FIRST_NAME_CLAIM).asString())
                                .lastName(decodedJWT.getClaim(LAST_NAME_CLAIM).asString())
                                .email(decodedJWT.getClaim(EMAIL_CLAIM).asString())
                                .role(Role.valueOf(decodedJWT.getClaim(ROLE_CLAIM).asString()))
                                .build();

                        CustomUserDetails userDetails = new CustomUserDetails(account);
                        UsernamePasswordAuthenticationToken authenticationToken =
                                new UsernamePasswordAuthenticationToken(token, null, userDetails.getAuthorities());
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }
            }
            filterChain.doFilter(request, response);
    }
}
