package org.example.config.security.details;

import lombok.RequiredArgsConstructor;
import org.example.models.Account;
import org.example.repositories.AccountRepository;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.example.utils.StringUtils.USER_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Account> account = accountRepository.findByEmail(email);
        if (account.isEmpty())
            throw new UsernameNotFoundException(USER_NOT_FOUND);
        return new CustomUserDetails(account.get());
    }
}
