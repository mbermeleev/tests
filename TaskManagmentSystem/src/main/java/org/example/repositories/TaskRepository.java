package org.example.repositories;

import org.example.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Modifying
    @Query("""
            DELETE FROM #{#entityName} t
            WHERE t.id = :id
            AND t.author.id = :authorId
            """)
    void deleteByIdAndAuthorId(@Param("id")Long id, @Param("authorId")Long authorId);

    @Modifying
    @Query("""
            SELECT t FROM #{#entityName} t
            WHERE t.id > :id
            AND t.executor.id = :executorID
            ORDER BY t.id""")
    List<Task> findTasksFromByExecutor(@Param("id")Long id, @Param("executorID") Long executorID);

    @Modifying
    @Query("""
            SELECT t FROM #{#entityName} t
            WHERE t.id > :id
            AND t.author.id = :authorID
            ORDER BY t.id""")
    List<Task> findTasksFromByAuthor(@Param("id")Long id, @Param("authorID") Long authorID);

    @Modifying
    @Query("""
            UPDATE #{#entityName} t
            SET t.status = :status
            WHERE t.id = :taskID
            AND (t.author.id = :accountID OR t.executor.id = :accountId)""")
    int updateStatusByAuthorIdOrExecutorId(@Param("status") Integer status, @Param("accountID") Long accountID, @Param("taskID") Long TaskID);


    @Modifying
    @Query("""
            UPDATE #{#entityName} t
            SET t.executor.id = :executorID
            WHERE t.id = :taskID
            AND t.author.id = :accountID""")
    int updateExecutor(@Param("executorID") Long executorID, @Param("taskID") Long TaskID, @Param("accountID") Long accountID);
}
