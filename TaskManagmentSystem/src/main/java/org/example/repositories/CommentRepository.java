package org.example.repositories;

import org.example.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Modifying
    @Query("""
            SELECT c FROM #{#entityName} c
            WHERE c.task.id = :taskId
            ORDER BY c.id
            """)
    List<Comment> findCommentsByTaskId(@Param("taskId") Long taskId);
}
