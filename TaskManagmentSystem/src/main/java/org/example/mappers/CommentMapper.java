package org.example.mappers;

import org.example.DTOs.CommentDTO;
import org.example.models.*;

import java.time.Instant;

public class CommentMapper {

    public static CommentDTO toDTO(Comment comment) {
        return CommentDTO.builder()
                .id(comment.getId())
                .text(comment.getText())
                .createDate(comment.getCreateDate())
                .authorNameAndInitial(comment.getAuthorNameAndInitial())
                .authorID(comment.getAuthor().getId())
                .taskID(comment.getTask().getId())
                .build();
    }

    public static Comment toEntity(CommentDTO commentDTO) {
        return Comment.builder()
                .id(commentDTO.getId())
                .text(commentDTO.getText())
                .createDate(commentDTO.getCreateDate())
                .authorID(commentDTO.getAuthorID())
                .taskID(commentDTO.getTaskID())
                .build();
    }
}
