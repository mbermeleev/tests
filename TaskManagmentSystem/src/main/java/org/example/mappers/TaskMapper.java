package org.example.mappers;

import org.example.DTOs.TaskDTO;
import org.example.enums.Status;
import org.example.models.Task;

import java.util.stream.Collectors;

public class TaskMapper {

    public static TaskDTO toDTO(Task task) {
        return TaskDTO.builder()
                .id(task.getId())
                .title(task.getTitle())
                .description(task.getDescription())
                .statusCode(task.getStatus().getStatusCode())
                .priority(task.getPriority())
                .createDate(task.getCreateDate().toEpochMilli())
                .authorID(task.getAuthorID())
                .authorNameAndInitial(task.getAuthor().getUserNameAndInitial())
                .executorID(task.getExecutorID())
                .executorNameAndInitial(task.getExecutor().getUserNameAndInitial())
                .comments(task.getComments()
                        .stream()
                        .limit(10)
                        .map(CommentMapper::toDTO)
                        .collect(Collectors.toList()))
                .build();
    }

    public static Task toEntity(TaskDTO taskDTO) {
        return Task.builder()
                .id(taskDTO.getId())
                .title(taskDTO.getTitle())
                .description(taskDTO.getDescription())
                .status(getStatusEnum(taskDTO.getStatusCode()))
                .priority(taskDTO.getPriority())
                .authorID(taskDTO.getAuthorID())
                .executorID(taskDTO.getExecutorID())
                .build();
    }

    private static Status getStatusEnum(int statusCode) {
        Status status;

        switch (statusCode) {
            case 1 -> status = Status.CREATED;
            case 2 -> status = Status.IN_PROGRESS;
            case 3 -> status = Status.PENDING;
            case 4 -> status = Status.COMPLETED;
            default -> status = Status.NONE;
        }
        return status;
    }
}
