package org.example.DTOs;

public record AccountAuthenticationDTO(String email, String password) {

    @Override
    public String email() {
        return email;
    }

    @Override
    public String password() {
        return password;
    }
}
