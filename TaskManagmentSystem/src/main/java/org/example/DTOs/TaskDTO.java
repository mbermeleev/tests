package org.example.DTOs;

import lombok.*;
import org.example.enums.Priority;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {

    private Long id;
    private String title;
    private String description;
    private Integer statusCode;
    private Long createDate;
    private Priority priority;
    private Long authorID;
    private Long executorID;
    private String authorNameAndInitial;
    private String executorNameAndInitial;
    private List<CommentDTO> comments;
}
