package org.example.DTOs;

import lombok.*;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private Long id;
    private String text;
    private Instant createDate;
    private String authorNameAndInitial;
    private Long authorID;
    private Long taskID;
}
