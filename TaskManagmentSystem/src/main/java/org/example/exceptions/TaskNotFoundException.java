package org.example.exceptions;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Задача не найдена!");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
