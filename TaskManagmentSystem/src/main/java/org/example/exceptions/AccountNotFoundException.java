package org.example.exceptions;

import org.example.utils.StringUtils;

public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException() {
        super(StringUtils.USER_NOT_FOUND);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
