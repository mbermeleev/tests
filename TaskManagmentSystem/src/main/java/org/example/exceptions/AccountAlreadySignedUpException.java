package org.example.exceptions;

public class AccountAlreadySignedUpException extends RuntimeException{

    public AccountAlreadySignedUpException() {
        super("Пользователь с таким email уже зарегистрирован");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
