package org.example.exceptions;

public class NotEnoughRightsException extends RuntimeException {
    public NotEnoughRightsException() {
        super("Недостаточно прав, для совершения операции!");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
