package org.example.exceptions;

import org.example.utils.StringUtils;

public class NotValidEmailException extends RuntimeException {

    public NotValidEmailException() {
        super(StringUtils.NOT_VALID_EMAIL);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
