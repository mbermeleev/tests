package org.example.utils;

public class StringUtils {

    public static final String USER_NOT_FOUND = "Пользователь с таким логином/паролем не найден!";
    public static final String BEARER = "Bearer ";
    public static final String AUTHORIZATION = "Authorization";
    public static final String INVALID_JWT_TOKEN_IN_BEARER = "Invalid JWT token in Bearer header";
    public static final String TOKEN_EXPIRED = "Токен просрочен, необходимо пройти авторизацию, для получения нового)";
    public static final String FAILED_REGISTRATION = "Неудачная попытка регистрации, пожалуйста повторите";
    public static final String TASK_HAS_DELETE = "Задача была удалена";
    public static final String TASK_STATUS_CHANGE_SUCCESS = "Статус был успешно изменен";
    public static final String TASK_EXECUTOR_CHANGE_SUCCESS = "Исполнитель был успешно изменен";
    public static final String NOT_VALID_EMAIL = "Email не может начинаться с символа @. Email должен содержать на конце @email.ru";
}
