package org.example.utils;

public class SecurityUtils {

    public static final String ID_CLAIM = "id";
    public static final String FIRST_NAME_CLAIM = "firstName";
    public static final String LAST_NAME_CLAIM = "lastName";
    public static final String EMAIL_CLAIM = "eMail";
    public static final String ROLE_CLAIM = "role";
    public static final String SECRET_KEY = "SECRET";
}
