package org.example.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.example.DTOs.TaskDTO;
import org.example.exceptions.*;
import org.example.mappers.TaskMapper;
import org.example.repositories.TaskRepository;
import org.example.services.interfaces.TaskService;
import org.example.utils.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    @Override
    @Transactional
    public HttpStatus addNewTask(TaskDTO taskDTO) {
        try {
            var newTask = TaskMapper.toEntity(taskDTO);
            newTask.setCreateDate(Instant.now());

            taskRepository.save(newTask);
            return HttpStatus.OK;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @Override
    @Transactional
    public TaskDTO editTask(TaskDTO taskDTO, String token) {
        return TaskMapper.toDTO(taskRepository.save(TaskMapper.toEntity(taskDTO)));
    }

    @Override
    @Transactional
    public String deleteTask(Long taskID, String token) {
        Long accountID = getaAccountID(token);

        taskRepository.deleteByIdAndAuthorId(taskID, accountID);

        var task = taskRepository.findById(taskID);
        if (task.isEmpty()) {
            return StringUtils.TASK_HAS_DELETE;
        } else {
            throw new NotEnoughRightsException();
        }
    }

    @Override
    @Transactional
    public TaskDTO getTask(Long taskID) {
        return TaskMapper.toDTO(taskRepository.findById(taskID).orElseThrow(TaskNotFoundException::new));
    }

    @Override
    @Transactional
    public List<TaskDTO> getTasksByExecutor(Long executorID, Long firstTaskID) {
        return taskRepository.findTasksFromByExecutor(firstTaskID, executorID)
                .stream()
                .limit(30)
                .map(TaskMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<TaskDTO> getTasksByAuthor(Long authorID, Long firstTaskID) {
        return taskRepository.findTasksFromByAuthor(firstTaskID, authorID)
                .stream()
                .limit(30)
                .map(TaskMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public String changeStatus(Long taskID, Integer statusCode, String token) {

        var accountID = getaAccountID(token);

        if (taskRepository.updateStatusByAuthorIdOrExecutorId(statusCode, accountID, taskID) > 0) {
            return StringUtils.TASK_STATUS_CHANGE_SUCCESS;
        } else {
            throw new NotEnoughRightsException();
        }
    }

    @Override
    @Transactional
    public String changeExecutor(Long taskID, Long executorId, String token) {

        var accountID = getaAccountID(token);

        var result = taskRepository.updateExecutor(executorId, taskID, accountID);

        if (result > 0) {
            return StringUtils.TASK_EXECUTOR_CHANGE_SUCCESS;
        } else {
            throw new NotEnoughRightsException();
        }
    }

    private Long getaAccountID(String token) {
        DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SecurityUtils.SECRET_KEY))
                .build().verify(token);

        return decodedJWT.getClaim(SecurityUtils.ID_CLAIM).asLong();
    }
}
