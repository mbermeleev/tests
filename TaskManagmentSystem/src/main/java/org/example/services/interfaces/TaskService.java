package org.example.services.interfaces;

import org.example.DTOs.TaskDTO;
import org.springframework.http.HttpStatus;

import java.util.List;

public interface TaskService {
    HttpStatus addNewTask(TaskDTO taskDTO);

    TaskDTO editTask(TaskDTO taskDTO, String token);

    String deleteTask(Long taskID, String token);

    TaskDTO getTask(Long taskID);

    List<TaskDTO> getTasksByExecutor(Long executorID, Long firstTaskID);

    List<TaskDTO> getTasksByAuthor(Long authorID, Long firstTaskID);

    String changeStatus(Long taskID, Integer statusCode, String token);

    String changeExecutor(Long taskID, Long executorId, String token);
}
