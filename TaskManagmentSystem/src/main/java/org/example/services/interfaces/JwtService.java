package org.example.services.interfaces;

import org.example.DTOs.AccountAuthenticationDTO;

public interface JwtService {

    String generateToken(AccountAuthenticationDTO accountAuthenticationDTO);
}
