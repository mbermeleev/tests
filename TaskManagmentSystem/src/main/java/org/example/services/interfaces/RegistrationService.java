package org.example.services.interfaces;

import org.example.DTOs.SignUpDto;

public interface RegistrationService {

    Boolean signUp(SignUpDto signUpDto);
}
