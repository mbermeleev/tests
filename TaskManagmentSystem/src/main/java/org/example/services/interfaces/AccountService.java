package org.example.services.interfaces;

import org.example.DTOs.*;

public interface AccountService {
    String accountAuthenticate(AccountAuthenticationDTO accountAuthenticationDTO);
    String signUp(SignUpDto signUpDto);
}
