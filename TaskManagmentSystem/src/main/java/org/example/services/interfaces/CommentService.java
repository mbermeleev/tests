package org.example.services.interfaces;

import org.example.DTOs.CommentDTO;

import java.util.List;

public interface CommentService {
    CommentDTO addNewComment(CommentDTO commentDTO);

    List<CommentDTO> getNextComments(Long taskID, Long lastCommentID);
}
