package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.DTOs.CommentDTO;
import org.example.mappers.CommentMapper;
import org.example.repositories.AccountRepository;
import org.example.repositories.CommentRepository;
import org.example.repositories.TaskRepository;
import org.example.services.interfaces.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final AccountRepository accountRepository;
    private final TaskRepository taskRepository;

    @Override
    @Transactional
    public CommentDTO addNewComment(CommentDTO commentDTO) {
        var newComment = CommentMapper.toEntity(commentDTO);
        if (newComment.getCreateDate() == null)//заглушка для тестов(((, не придумал, как это дело обойти
            newComment.setCreateDate(Instant.now());
        newComment.setAuthor(accountRepository.findById(commentDTO.getAuthorID()).get());
        newComment.setTask(taskRepository.findById(commentDTO.getTaskID()).get());
        return CommentMapper.toDTO(commentRepository.save(newComment));
    }

    @Override
    @Transactional
    public List<CommentDTO> getNextComments(Long taskID, Long lastCommentID) {
        return commentRepository.findCommentsByTaskId(taskID)
                .stream()
                .filter(it -> it.getId() > lastCommentID)
                .limit(10)
                .map(CommentMapper::toDTO)
                .collect(Collectors.toList());
    }
}
