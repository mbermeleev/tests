package org.example.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import org.example.DTOs.AccountAuthenticationDTO;
import org.example.exceptions.AccountNotFoundException;
import org.example.repositories.AccountRepository;
import org.example.services.interfaces.JwtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.Date;

import static org.example.utils.SecurityUtils.*;

@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {
    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public String generateToken(AccountAuthenticationDTO accountAuthenticationDTO) {
        var account = accountRepository.findByEmail(accountAuthenticationDTO.email())
                .orElseThrow(AccountNotFoundException::new);
        Instant expirationDate = Instant.from(ZonedDateTime.now().plusHours(1).toInstant());
        return JWT.create()
                .withClaim(ID_CLAIM, account.getId())
                .withClaim(EMAIL_CLAIM, account.getEmail())
                .withClaim(FIRST_NAME_CLAIM, account.getFirstName())
                .withClaim(LAST_NAME_CLAIM, account.getLastName())
                .withClaim(ROLE_CLAIM, String.valueOf(account.getRole()))
                .withIssuedAt(new Date())
                .withExpiresAt(expirationDate)
                .sign(Algorithm.HMAC256(SECRET_KEY));
    }
}
