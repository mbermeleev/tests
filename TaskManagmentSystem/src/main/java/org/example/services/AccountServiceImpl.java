package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.DTOs.*;
import org.example.exceptions.AccountAlreadySignedUpException;
import org.example.services.interfaces.*;
import org.example.utils.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final JwtService jwtService;
    private final RegistrationService registrationService;

    @Override
    public String accountAuthenticate(AccountAuthenticationDTO accountAuthenticationDTO) {
        return jwtService.generateToken(accountAuthenticationDTO);
    }

    @Override
    public String signUp(SignUpDto signUpDto) {

        try {
            if (registrationService.signUp(signUpDto)) {
                return jwtService.generateToken(new AccountAuthenticationDTO(signUpDto.email(), signUpDto.password()));
            } else {
                return StringUtils.FAILED_REGISTRATION;
            }
        } catch(AccountAlreadySignedUpException e) {
            return e.getMessage();
        }
    }
}
