package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.DTOs.SignUpDto;
import org.example.enums.Role;
import org.example.exceptions.AccountAlreadySignedUpException;
import org.example.exceptions.NotValidEmailException;
import org.example.models.Account;
import org.example.repositories.AccountRepository;
import org.example.services.interfaces.RegistrationService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Boolean signUp(SignUpDto signUpDto) {
        if (!signUpDto.email().contains("@mail.ru") || signUpDto.email().startsWith("@")) {
            throw new NotValidEmailException();
        }
        if (accountRepository.findByEmail(signUpDto.email()).isEmpty()) {
            accountRepository.save(Account.builder()
                            .firstName(signUpDto.firstName())
                            .lastName(signUpDto.lastName())
                            .email(signUpDto.email())
                            .password(passwordEncoder.encode(signUpDto.password()))
                            .role(Role.USER)
                            .build());
            return true;
        } else {
            throw new AccountAlreadySignedUpException();
        }
    }
}
