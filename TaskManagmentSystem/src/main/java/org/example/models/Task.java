package org.example.models;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.*;
import org.example.enums.*;

import java.time.Instant;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private Status status;
    private Priority priority;
    private Instant createDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "author_id")
    private Account author;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "executor_id")
    private Account executor;

    @OneToMany(cascade = CascadeType.ALL)
    @Nullable
    private List<Comment> comments;

    @Transient
    private Long authorID;
    @Transient
    private Long executorID;

    public Long getAuthorID() {
        return author.getId();
    }

    public Long getExecutorID() {
        return executor.getId();
    }
}
