package org.example.models;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.*;
import org.example.enums.Role;

import java.util.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Role role = Role.USER;

    @OneToMany(cascade = CascadeType.ALL)
    @Nullable
    private List<Task> createdByUser = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @Nullable
    private List<Task> distributedToUser = new ArrayList<>();

    @Transient
    private String usersNameAndInitial;

    public String getUserNameAndInitial() {
        return firstName.concat(String.valueOf(lastName.charAt(0))).concat(".");
    }
}
