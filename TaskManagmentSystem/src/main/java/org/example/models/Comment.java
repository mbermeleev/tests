package org.example.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 1000)
    private String text;
    private Instant createDate = Instant.now();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "author_id")
    private Account author;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "task_id")
    private Task task;

    @Transient
    private String authorNameAndInitial;
    @Transient
    private Long taskID;
    @Transient
    private Long authorID;

    public String getAuthorNameAndInitial() {
        return author.getUserNameAndInitial();
    }

    public Long getTaskID() {
        return taskID;
    }

    public Long getAuthorID() {
        return authorID;
    }
}
