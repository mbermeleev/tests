package org.example.services;

import org.example.DTOs.CommentDTO;
import org.example.enums.*;
import org.example.models.*;
import org.example.repositories.*;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/test_properties.yml")
class CommentServiceImplTest {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentServiceImpl commentService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TaskRepository taskRepository;

    private Account author;

    private Task task;
    @BeforeEach
    public void setUp() {
        author = Account.builder()
                .firstName("account")
                .lastName("account")
                .email("email")
                .password("password")
                .role(Role.USER)
                .build();
        author = accountRepository.save(author);

        task = Task.builder()
                .title("title")
                .description("description")
                .priority(Priority.HIGH)
                .status(Status.IN_PROGRESS)
                .createDate(Instant.now())
                .author(author)
                .executor(author)
                .build();

        task = taskRepository.save(task);

        CommentDTO commentDTO1 = createSampleCommentDTO(author, task);
        CommentDTO commentDTO2 = createSampleCommentDTO(author, task);

        commentService.addNewComment(commentDTO1);
        commentService.addNewComment(commentDTO2);
    }


    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addNewComment() is working when")
    class ForAddNewComment {

        @Test
        @Transactional
        public void comments_added_to_db() {
            CommentDTO commentDTO = createSampleCommentDTO(author, task);

            CommentDTO savedCommentDTO = commentService.addNewComment(commentDTO);

            assertNotNull(savedCommentDTO.getId());
            assertEquals(commentDTO.getText(), savedCommentDTO.getText());
            assertNotNull(savedCommentDTO.getCreateDate());
        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("getNextComments() is working when")
    class ForGetNextComments {

        @Test
        @Transactional
        void next_comments_return_good_results() {
            List<CommentDTO> nextComments = commentService.getNextComments(2L, 0L);

            assertEquals(2, nextComments.size());
            assertEquals(4L, nextComments.get(0).getId());
            assertEquals(5L, nextComments.get(1).getId());
        }
    }

    private CommentDTO createSampleCommentDTO(Account author, Task task) {
        return CommentDTO.builder()
                .text("Test comment")
                .authorID(author.getId())
                .taskID(task.getId())
                .build();
    }
}