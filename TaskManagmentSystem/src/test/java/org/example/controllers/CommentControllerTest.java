package org.example.controllers;

import org.example.DTOs.CommentDTO;
import org.example.services.CommentServiceImpl;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource("/test_properties.yml")
class CommentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentServiceImpl commentService;

    @BeforeEach
    public void setUp() {
        CommentDTO commentDTO = CommentDTO.builder()
                .text("Test comment")
                .taskID(1L)
                .authorID(1L)
                .createDate(Instant.now())
                .build();

        CommentDTO savedComment = CommentDTO.builder()
                .id(11L)
                .text("Test comment")
                .authorNameAndInitial("JohnD.")
                .taskID(1L)
                .authorID(1L)
                .createDate(commentDTO.getCreateDate())
                .build();

        List<CommentDTO> listForReturn = Collections.singletonList(savedComment);

        when(commentService.addNewComment(commentDTO)).thenReturn(savedComment);
        when(commentService.getNextComments(1L, 10L)).thenReturn(listForReturn);
    }


    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addNewComment() is working")
    class ForAddNewComment {

        @Test
        @WithMockUser(username = "testuser")
        void return_right_result() throws Exception {
            SecurityContextHolder.clearContext();
            mockMvc.perform(post("/comment/addNewComment")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("""
                                    {
                                      "text" : "Test comment",
                                      "taskID" : 1,
                                      "authorID" : 1
                                    }"""))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("getNextComments() is working when")
    class ForGetNextComments {

        @Test
        @WithMockUser(username = "testuser")
        void return_right_result() throws Exception {
            SecurityContextHolder.clearContext();
            mockMvc.perform(get("/comment/getNextComments/{task_id}/{last_comment_id}", 1, 10))
                    .andDo(print())
                    .andExpect(status().isOk());
        }

        @Test
        void return_403_status_code_without_authorisation() throws Exception {
            SecurityContextHolder.clearContext();
            mockMvc.perform(get("/comment/getNextComments/{task_id}/{last_comment_id}", 1, 10))
                    .andDo(print())
                    .andExpect(status().isForbidden());
        }
    }
}