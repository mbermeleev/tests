package org.example.repositories;

import org.example.DTOs.CommentDTO;
import org.example.enums.Priority;
import org.example.enums.Role;
import org.example.enums.Status;
import org.example.models.Account;
import org.example.models.Task;
import org.example.services.CommentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/test_properties.yml")
class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentServiceImpl commentService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TaskRepository taskRepository;

    @BeforeEach
    public void setUp() {
        Account author = Account.builder()
                .firstName("account")
                .lastName("account")
                .email("email")
                .password("password")
                .role(Role.USER)
                .build();
        author = accountRepository.save(author);

        Task task = Task.builder()
                .title("title")
                .description("description")
                .priority(Priority.HIGH)
                .status(Status.IN_PROGRESS)
                .createDate(Instant.now())
                .author(author)
                .executor(author)
                .build();

        task = taskRepository.save(task);

        CommentDTO commentDTO1 = createSampleCommentDTO(author, task);
        CommentDTO commentDTO2 = createSampleCommentDTO(author, task);

        commentService.addNewComment(commentDTO1);
        commentService.addNewComment(commentDTO2);
    }


    @Test
    @Transactional
    void findCommentsByTaskId() {
        List<CommentDTO> nextComments = commentService.getNextComments(1L, 0L);

        assertEquals(2, nextComments.size());
    }

    private CommentDTO createSampleCommentDTO(Account author, Task task) {
        return CommentDTO.builder()
                .text("Test comment")
                .authorID(author.getId())
                .taskID(task.getId())
                .build();
    }
}