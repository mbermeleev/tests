package org.example.trafficcop.servicesImpl;

import org.example.trafficcop.exceptions.DataBaseFilledException;
import org.example.trafficcop.models.Number;
import org.example.trafficcop.repository.NumbersRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.data.domain.*;
import org.springframework.data.repository.query.FluentQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumberServiceImpl is working when")
class NumbersServiceImplTest {

    @DisplayName(("getNextNumber() is working when"))
    @Nested
    class ForGetNextNumber {

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {1_728_000, 2_100_000})
        public void on_problems_repeats(int repeats) {
            assertThrows(DataBaseFilledException.class, () -> numbersService.getNextNumber("A111AA", repeats));
        }
        @ParameterizedTest(name = "change third letter on {0}")
        @ValueSource(strings = "А999АА")
        public void on_number_with_999(String number) {
            String newNumber = numbersService.getNextNumber(number, 0);
            assertEquals("А000АВ", newNumber);
        }

        @ParameterizedTest(name = "change second letter on {0}")
        @ValueSource(strings = "A999AX")
        public void on_number_which_need_to_change_second_number(String number) {
            String newNumber = numbersService.getNextNumber(number, 12_000);
            assertEquals("A000BA", newNumber);
        }

        @ParameterizedTest(name = "change first letter on {0}")
        @ValueSource(strings = "А999ХХ")
        public void on_number_which_need_to_change_first_number(String number) {
            String newNumber = numbersService.getNextNumber(number,144_000);
            assertEquals("В000АА", newNumber);
        }

    }
    @DisplayName(("getNextChar() is working when"))
    @Nested
    class ForGetNextChar {

        private static final List<String> EXPECTED = Arrays.asList("В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х", "А");
        private static final List<String> LETTERS = Arrays.asList("А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х");

        @Test
        @DisplayName("Test for each char is working")
        public void on_each_of_letters() {
            ArrayList<String> strings = new ArrayList<>();
            for (String letter : LETTERS) {
                String newLetter = numbersService.getNextChar(letter);
                strings.add(newLetter);
            }
            assertEquals(EXPECTED, strings);
        }

    }
    private NumbersServiceImpl numbersService = new NumbersServiceImpl(new NumbersRepository() {
        @Override
        public String getNumberByGosNumber(String number) {
            return null;
        }

        @Override
        public String getNumberById() {
            return null;
        }

        @Override
        public void flush() {

        }

        @Override
        public <S extends Number> S saveAndFlush(S entity) {
            return null;
        }

        @Override
        public <S extends Number> List<S> saveAllAndFlush(Iterable<S> entities) {
            return null;
        }

        @Override
        public void deleteAllInBatch(Iterable<Number> entities) {

        }

        @Override
        public void deleteAllByIdInBatch(Iterable<Long> longs) {

        }

        @Override
        public void deleteAllInBatch() {

        }

        @Override
        public Number getOne(Long aLong) {
            return null;
        }

        @Override
        public Number getById(Long aLong) {
            return null;
        }

        @Override
        public Number getReferenceById(Long aLong) {
            return null;
        }

        @Override
        public <S extends Number> List<S> findAll(Example<S> example) {
            return null;
        }

        @Override
        public <S extends Number> List<S> findAll(Example<S> example, Sort sort) {
            return null;
        }

        @Override
        public <S extends Number> List<S> saveAll(Iterable<S> entities) {
            return null;
        }

        @Override
        public List<Number> findAll() {
            return null;
        }

        @Override
        public List<Number> findAllById(Iterable<Long> longs) {
            return null;
        }

        @Override
        public <S extends Number> S save(S entity) {
            return null;
        }

        @Override
        public Optional<Number> findById(Long aLong) {
            return Optional.empty();
        }

        @Override
        public boolean existsById(Long aLong) {
            return false;
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public void deleteById(Long aLong) {

        }

        @Override
        public void delete(Number entity) {

        }

        @Override
        public void deleteAllById(Iterable<? extends Long> longs) {

        }

        @Override
        public void deleteAll(Iterable<? extends Number> entities) {

        }

        @Override
        public void deleteAll() {

        }

        @Override
        public List<Number> findAll(Sort sort) {
            return null;
        }

        @Override
        public Page<Number> findAll(Pageable pageable) {
            return null;
        }

        @Override
        public <S extends Number> Optional<S> findOne(Example<S> example) {
            return Optional.empty();
        }

        @Override
        public <S extends Number> Page<S> findAll(Example<S> example, Pageable pageable) {
            return null;
        }

        @Override
        public <S extends Number> long count(Example<S> example) {
            return 0;
        }

        @Override
        public <S extends Number> boolean exists(Example<S> example) {
            return false;
        }

        @Override
        public <S extends Number, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
            return null;
        }
    });
}
