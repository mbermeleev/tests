package org.example.trafficcop.services;

public interface NumbersService {

    String getRandomNumber();
    String getNextNumber();

    void save(String number);
}
