package org.example.trafficcop.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.trafficcop.exceptions.DataBaseFilledException;
import org.example.trafficcop.models.Number;
import org.example.trafficcop.repository.NumbersRepository;
import org.example.trafficcop.services.NumbersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Random;

import static org.example.trafficcop.utils.AnotherUtils.*;

@Service
@RequiredArgsConstructor
public class NumbersServiceImpl implements NumbersService {

    private final NumbersRepository numbersRepository;

    @Override
    @Transactional
    public String getRandomNumber() {
        String newNumber = getNumberWithoutRegionForRandom();
        return getNotRepeatAndSaveNewNumber(newNumber);
    }
    
    @Override
    @Transactional
    public String getNextNumber() {
        String lastNumberFromDB = numbersRepository.getNumberById();
        return getNotRepeatAndSaveNewNumber(lastNumberFromDB);
    }

    @Override
    @Transactional
    public void save(String number) {
        numbersRepository.save(Number.builder()
                .gosNumber(number)
                .saveTime(Instant.now())
                .build());
    }

    public String getNotRepeatAndSaveNewNumber(String newNumber) {
        int repeats = 0;

        String repeatNumber = numbersRepository.getNumberByGosNumber(newNumber);

        while (repeatNumber != null) {
            newNumber = getNextNumber(newNumber, repeats);
            repeatNumber = numbersRepository.getNumberByGosNumber(newNumber);

            repeats++;
        }

        save(newNumber);

        return newNumber + " " + regionNumber;
    }

    public String getNumberWithoutRegionForRandom() {
        int randomNumber = new Random().nextInt(999);
        int firstCharNumber = new Random().nextInt(12) - 1;
        int secondCharNumber = new Random().nextInt(12) - 1;
        int thirdCharNumber = new Random().nextInt(12) - 1;

        String firsChar = characters[firstCharNumber];
        String secondChar = characters[secondCharNumber];
        String thirdChar = characters[thirdCharNumber];

        return firsChar + randomNumber + secondChar + thirdChar;
    }

    public String getNextNumber(String number, int repeats) {

        String onlyDigits = number.substring(1, 4);
        String nines = "999";
        String firstChar = String.valueOf(number.charAt(0));
        String secondChar = String.valueOf(number.charAt(4));
        String thirdChar = String.valueOf(number.charAt(5));

        boolean needToChangeFirstChar = (repeats % (1000 * 12 * 12) == 0 && repeats != 0);
        boolean needToChangeSecondChar = (repeats % (1000 * 12) == 0  && repeats != 0);
        boolean needToChangeThirdChar = ((repeats % 1000 == 0 && repeats != 0) || onlyDigits.equals(nines));
        boolean isDBFilled = repeats >= 1000 * 12 * 12 * 12;


        if (isDBFilled) {
            throw new DataBaseFilledException();
        }

        if (needToChangeFirstChar) {
            firstChar = getNextChar(firstChar);
            secondChar = getNextChar(secondChar);
            thirdChar = getNextChar(thirdChar);
        } else if (needToChangeSecondChar) {
            secondChar = getNextChar(secondChar);
            thirdChar = getNextChar(thirdChar);
        } else if (needToChangeThirdChar) {
            thirdChar = getNextChar(thirdChar);
        }

        if (onlyDigits.equals(nines)) {
            onlyDigits = "000";
        } else {
            int digitsFromString = Integer.parseInt(onlyDigits);
            onlyDigits = String.valueOf(digitsFromString + 1);
            if (onlyDigits.length() == 1) {
                onlyDigits = "00" + onlyDigits;
            } else if (onlyDigits.length() == 2) {
                onlyDigits = "0" + onlyDigits;
            }
        }

        String forReturn = "";
        return forReturn.concat(firstChar)
                .concat(onlyDigits)
                .concat(secondChar)
                .concat(thirdChar);
    }

    public String getNextChar(String character) {
        int index = 0;

        for (int i = 0; i < characters.length; i++) {
            if (character.equals(characters[i])) {
                index = i;
                break;
            }
        }
        if (index == characters.length - 1) {
            character = characters[0];
        } else {
            character = characters[index + 1];
        }
        return character;
    }
}
