package org.example.trafficcop.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.trafficcop.services.NumbersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/numbers")
@Tag(name = "Контроллер для получения нового номера",
        description = "Позволяет получить либо случайный, либо следующий от последнего полученного номера")
public class NumberController {

    private final NumbersService numbersService;

    @RequestMapping("/random")
    @Operation(
            summary = "Получение случайного номера",
            description = "Метод позволяет получить случайный номер," +
                    " если такой номер уже есть в базе данных, то получаем следующий, от случайного"
    )
    public ResponseEntity<String> getRandomNumber() {
        return ResponseEntity.ok().body(numbersService.getRandomNumber());
    }

    @RequestMapping("/next")
    @Operation(
            summary = "Получение следующего номера",
            description = "Метод позволяет получить номер номер от последнего полученного," +
                    " если такой номер уже есть в базе данных, то получаем следующий, от случайного"
    )
    public ResponseEntity<String> getNextNumber() {
        return ResponseEntity.ok().body(numbersService.getNextNumber());
    }
}
