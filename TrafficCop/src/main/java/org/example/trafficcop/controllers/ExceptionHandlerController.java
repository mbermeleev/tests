package org.example.trafficcop.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.trafficcop.exceptions.DataBaseFilledException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Hidden
@Tag(name = "Контроллер обработки исключений",
        description = "Позволяет обработать собственные исключения")
public class ExceptionHandlerController {

    @Operation(
            summary = "Обработка ошибки переполнения базы данных",
            description = "Позволяет получить сообщение о переполненности базы данных"
    )
    @ExceptionHandler(DataBaseFilledException.class)
    public ResponseEntity<DataBaseFilledException> personNotFoundException() {
        return ResponseEntity.status(999).body(new DataBaseFilledException());
    }
}
