package org.example.trafficcop.config.openapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(
                title = "Traffic Cop",
                version = "1.0.0",
                contact = @Contact(
                        name = "Бермелеев Марат",
                        email = "mbermeleev@bk.ru"
                )
        )
)
@RequiredArgsConstructor
@Configuration
public class OpenApiConfig {
}
