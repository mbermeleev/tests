package org.example.trafficcop.exceptions;

public class DataBaseFilledException extends RuntimeException {
    public DataBaseFilledException() {
        super("База данных заполнена! Необходимо поменять регион и создать новую базу данных для него!");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
