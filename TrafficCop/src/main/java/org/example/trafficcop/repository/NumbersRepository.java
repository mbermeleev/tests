package org.example.trafficcop.repository;

import org.example.trafficcop.models.Number;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface NumbersRepository extends JpaRepository<Number, Long> {

    @Modifying
    @Query("SELECT n.gosNumber FROM #{#entityName} n WHERE n.gosNumber = ?1")
    String getNumberByGosNumber(String number);

    @Modifying
    @Query("SELECT n.gosNumber FROM #{#entityName} n WHERE n.id=(SELECT max(n.id) FROM #{#entityName} n)")
    String getNumberById();
}
