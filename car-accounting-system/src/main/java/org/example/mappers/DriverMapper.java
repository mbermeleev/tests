package org.example.mappers;

import org.example.dto.DriverDto;
import org.example.models.Driver;
import org.mapstruct.Mapper;

@Mapper
public interface DriverMapper {
    DriverDto toDto(Driver driver);
    Driver toEntity(DriverDto driverDto);
}
