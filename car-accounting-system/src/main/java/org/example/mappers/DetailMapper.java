package org.example.mappers;

import org.example.dto.DetailDto;
import org.example.models.Detail;
import org.mapstruct.Mapper;

@Mapper
public interface DetailMapper {
    DetailDto toDto(Detail detail);
    Detail toEntity(DetailDto detailDto);
}
