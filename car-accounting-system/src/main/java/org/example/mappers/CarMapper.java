package org.example.mappers;

import org.example.dto.CarDto;
import org.example.models.Car;
import org.mapstruct.Mapper;

@Mapper
public interface CarMapper {
    CarDto toDto(Car car);
    Car toEntity(CarDto carDto);
}
