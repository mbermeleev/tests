package org.example.exceptions;

public class IncorrectNumberOfPageException extends RuntimeException {
    public IncorrectNumberOfPageException() {
        super("Номер страницы не может быть меньше 1!");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
