package org.example.exceptions;

public class DetailNotFoundException extends RuntimeException{
    public DetailNotFoundException() {
        super("Деталь не найдена!");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
