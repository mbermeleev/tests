package org.example.exceptions;

public class CarNotFoundException extends RuntimeException{
    public CarNotFoundException() {
        super("Автомобиль не найден в базе");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
