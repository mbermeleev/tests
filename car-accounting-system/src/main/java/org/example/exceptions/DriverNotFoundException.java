package org.example.exceptions;

public class DriverNotFoundException extends RuntimeException {

    public DriverNotFoundException() {
        super("Водитель с таким ID не найден");
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
