package org.example.config;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(
                title = "Car-Accounting-System",
                version = "1.0.0",
                contact = @Contact(
                        name = "Бермелеев Марат",
                        email = "mbermeleev@bk.ru"
                )
        )
)
@Configuration
public class OpenApiConfig {
}
