package org.example.services;

import org.example.dto.DetailDto;
import org.example.dto.DriverDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface CarsAndDriversService {
    String changeDriverFromCar(String vin, Long driverId);

    ArrayList<DetailDto> addNewDetailToCar(String vin, DetailDto detailDto);

    ArrayList<DetailDto> changeDetailInCar(Long detailIdWhichNeedToChange, String vin, DetailDto newDetail);

    ArrayList<DetailDto> deleteDetailFromCar(Long detailIdToDelete, String vin);
}
