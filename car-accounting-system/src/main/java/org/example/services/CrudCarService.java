package org.example.services;

import org.example.dto.CarDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface CrudCarService {
    CarDto getOneCar(String VIN);
    ArrayList<CarDto> getAllCars(int page);
    Boolean updateCar(CarDto carDto);
    Boolean saveCar(CarDto carDto);
    void deleteCar(String VIN);
}
