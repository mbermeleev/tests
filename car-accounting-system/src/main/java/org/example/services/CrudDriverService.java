package org.example.services;

import org.example.dto.DriverDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface CrudDriverService {
    DriverDto getOneDriver(Long id);
    ArrayList<DriverDto> getAllDrivers(int page);
    Boolean updateDriver(Long id, DriverDto driverDto);
    Boolean saveDriver(DriverDto driverDto);
    void deleteDriver(Long id);
}
