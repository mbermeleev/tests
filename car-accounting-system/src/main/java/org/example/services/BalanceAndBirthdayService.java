package org.example.services;

import org.example.models.Driver;
import org.example.parameters.ParametersBalance;
import org.springframework.stereotype.Service;

@Service
public interface BalanceAndBirthdayService {
    Double getBalance(Long driversId, String currencyType);
    Double changeBalance(Long driversId, Double increaseAmount, ParametersBalance parametersBalance);
    void sendCongratulation();
}
