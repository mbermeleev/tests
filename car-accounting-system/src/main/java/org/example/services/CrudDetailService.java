package org.example.services;

import org.example.dto.DetailDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface CrudDetailService {
    DetailDto getOne(Long id);
    ArrayList<DetailDto> getAll(int page);
    Boolean update(Long id, DetailDto detailDto);
    Boolean save(DetailDto detailDto);
    void delete(Long id);
}
