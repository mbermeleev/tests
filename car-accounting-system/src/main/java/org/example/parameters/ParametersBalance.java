package org.example.parameters;

import lombok.*;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Component
public class ParametersBalance {
    private Boolean isIncreaseBalance;
}
