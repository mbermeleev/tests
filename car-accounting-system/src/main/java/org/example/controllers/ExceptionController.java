package org.example.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Hidden
@Tag(name = "Контроллер обработки исключений",
        description = "Позволяет обработать собственные исключения")
public class ExceptionController {

    @Operation(
            summary = "Обработка ошибки поиска машины в базе",
            description = "Позволяет получить сообщение что машины нет в базе данных"
    )
    @ExceptionHandler(CarNotFoundException.class)
    public ResponseEntity<CarNotFoundException> carNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CarNotFoundException());
    }

    @Operation(
            summary = "Обработка ошибки поиска водителя в базе",
            description = "Позволяет получить сообщение что водителя нет в базе данных"
    )
    @ExceptionHandler(DriverNotFoundException.class)
    public ResponseEntity<DriverNotFoundException> driverNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new DriverNotFoundException());
    }

    @Operation(
            summary = "Обработка ошибки поиска детали в базе",
            description = "Позволяет получить сообщение что детали нет в базе данных"
    )
    @ExceptionHandler(DetailNotFoundException.class)
    public ResponseEntity<DetailNotFoundException> detailNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new DetailNotFoundException());
    }

    @Operation(
            summary = "Обработка ошибки ввода страницы",
            description = "Позволяет получить сообщение о том, что номер страницы меньше 1"
    )
    @ExceptionHandler(IncorrectNumberOfPageException.class)
    public ResponseEntity<IncorrectNumberOfPageException> incorrectNumberOfPageException() {
        return ResponseEntity.status(601).body(new IncorrectNumberOfPageException());
    }

    @Operation(
            summary = "Обработка ошибки ввода страницы",
            description = "Позволяет получить сообщение что номер страницы слишком велик"
    )
    @ExceptionHandler(VeryBigNumberOfPageException.class)
    public ResponseEntity<VeryBigNumberOfPageException> veryBigNumberOfPageException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new VeryBigNumberOfPageException());
    }
}
