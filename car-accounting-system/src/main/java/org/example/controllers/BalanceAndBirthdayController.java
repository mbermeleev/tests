package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.parameters.ParametersBalance;
import org.example.services.BalanceAndBirthdayService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name="Контроллер баланса и поздравления с днем рождения",
        description="Позволяет совершать операции с балансом и получать поздравления с днём рождения")
@RestController
@RequiredArgsConstructor
@RequestMapping("/balance_and_birthday")
public class BalanceAndBirthdayController {

    private final BalanceAndBirthdayService balanceAndBirthdayService;

    @Operation(
            summary = "Получение баланса счета у водителя",
            description = "Позволяет получить баланс счета водителя в любой валюте"
    )
    @GetMapping("/getBalance/{driversId}/{currencyType}")
    public ResponseEntity<Double> getBalance(@PathVariable @Parameter(description = "Id водителя")
                                                 Long driversId,
                                             @PathVariable @Parameter(description = "Тип валюты")
                                                 String currencyType){
        return ResponseEntity.ok().body(balanceAndBirthdayService.getBalance(driversId, currencyType));
    }

    @Operation(
            summary = "Увеличение баланса",
            description = "Позволяет увеличить баланс счета водителя"
    )
    @PatchMapping("/increaseBalance/{driversId}/{increaseAmount}")
    public ResponseEntity<Double> increaseBalance(@PathVariable @Parameter(description = "Id водителя")
                                                      Long driversId,
                                                  @PathVariable @Parameter(description = "Сумма на которую увеличиваем баланс")
                                                      Double increaseAmount) {
        return ResponseEntity.ok().body(balanceAndBirthdayService.changeBalance(
                driversId, increaseAmount,
                ParametersBalance.builder().isIncreaseBalance(true).build()));
    }

    @Operation(
            summary = "Уменьшаем баланс",
            description = "Позволяет уменьшить баланс счета водителя"
    )
    @PatchMapping("/increaseBalance/{driversId}/{reduceAmount}")
    public ResponseEntity<Double> reduceBalance(@PathVariable @Parameter(description = "Id водителя")
                                                    Long driversId,
                                                @PathVariable @Parameter(description = "Сумма на которую уменьшаем баланс")
                                                    Double reduceAmount) {
        return ResponseEntity.ok().body(balanceAndBirthdayService.changeBalance(
                driversId, reduceAmount,
                ParametersBalance.builder().isIncreaseBalance(false).build()));
    }
}
