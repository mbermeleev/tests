package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.DetailDto;
import org.example.dto.DriverDto;
import org.example.services.CarsAndDriversService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Tag(name="Контроллер автомобилей и водителей",
        description="Позволяет совершать операции связанные с автомобилями и водителями")
@RestController
@RequiredArgsConstructor
@RequestMapping("/cars_and_drivers")
public class CarsAndDriversController {

    private final CarsAndDriversService carsAndDriversService;

    @Operation(
            summary = "Замена водителя",
            description = "Позволяет сменить водителя у определенного автомобиля"
    )
    @PatchMapping("/changeDriverToCar/{VIN}/{driverId}")
    public ResponseEntity<String> changeDriverToCar(@PathVariable @Parameter(description = "VIN нужного автомобиля")
                                                           String VIN,
                                                       @PathVariable @Parameter(description = "Id водителя")
                                                           Long driverId) {
        return ResponseEntity.ok().body(carsAndDriversService.changeDriverFromCar(VIN, driverId));
    }

    @Operation(
            summary = "Добавление новой детали",
            description = "Позволяет добавить новую деталь в автомобиль и в базу данных"
    )
    @PutMapping("/addDetailToCar/{VIN}")
    public ResponseEntity<ArrayList<DetailDto>> addDetailToCar(@PathVariable @Parameter(description = "VIN нужного автомобиля")
                                                                   String VIN,
                                                               @RequestBody @Parameter(description = "Новая деталь")
                                                                   DetailDto detailDto) {
        return ResponseEntity.ok().body(carsAndDriversService.addNewDetailToCar(VIN, detailDto));
    }

    @Operation(
            summary = "Замена детали",
            description = "Позволяет заменить деталь в автомобиле на новую и сохранить новую в базу"
    )
    @PatchMapping("changeDetail/{detailIdWhichNeedToChange}/{VIN}")
    public ResponseEntity<ArrayList<DetailDto>> changeDetailInCar(@PathVariable @Parameter(description = "Id детали для замены")
                                                                      Long detailIdWhichNeedToChange,
                                                                  @PathVariable @Parameter(description = "VIN нужного автомобиля")
                                                                      String VIN,
                                                                  @RequestBody @Parameter(description = "Новая деталь")
                                                                      DetailDto newDetail) {
        return ResponseEntity.ok().body(carsAndDriversService.changeDetailInCar(detailIdWhichNeedToChange, VIN, newDetail));
    }

    @Operation(
            summary = "Удаление детали",
            description = "Позволяет удалить деталь из автомобиля"
    )
    @DeleteMapping("deleteDetailFromCar/{detailIdToDelete}/{VIN}")
    public ResponseEntity<ArrayList<DetailDto>> deleteDetailFromCar(@PathVariable @Parameter(description = "Id детали для удаления")
                                                                        Long detailIdToDelete,
                                                                    @PathVariable @Parameter(description = "VIN нужного автомобиля")
                                                                        String VIN) {
        return ResponseEntity.ok().body(carsAndDriversService.deleteDetailFromCar(detailIdToDelete, VIN));
    }
}
