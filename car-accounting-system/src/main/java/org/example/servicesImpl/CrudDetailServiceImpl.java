package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.DetailDto;
import org.example.exceptions.DetailNotFoundException;
import org.example.exceptions.IncorrectNumberOfPageException;
import org.example.exceptions.VeryBigNumberOfPageException;
import org.example.mappers.DetailMapper;
import org.example.models.Detail;
import org.example.repositories.DetailRepository;
import org.example.services.CrudDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CrudDetailServiceImpl implements CrudDetailService {

    private final DetailRepository detailRepository;
    private final DetailMapper detailMapper;

    @Override
    @Transactional
    public DetailDto getOne(Long id) {
        return detailMapper.toDto(detailRepository.findById(id).orElseThrow(DetailNotFoundException::new));
    }

    @Override
    @Transactional
    public ArrayList<DetailDto> getAll(int page) {
        ArrayList<DetailDto> details = (ArrayList<DetailDto>) detailRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Detail::getId))
                .map(detailMapper::toDto).toList();
        if (page < 1) {
            throw new IncorrectNumberOfPageException();
        }
        final int carsCountOnPage = 30;
        int firstCarOnPage = page > 1 ? ((carsCountOnPage * (page - 1)) - 1) : 0;
        int lastCarOnPage = (carsCountOnPage * page) - 1;

        return getLimitFeed(firstCarOnPage, lastCarOnPage, details);
    }

    @Override
    @Transactional
    public Boolean update(Long id, DetailDto detailDto) {
        var detail = detailRepository.findById(id).orElseThrow(DetailNotFoundException::new);
        detail.setCar(detailDto.getCar());
        detail.setName(detail.getName());
        detail.setSeriesNumber(detail.getSeriesNumber());
        detailRepository.save(detail);
        return true;
    }

    @Override
    @Transactional
    public Boolean save(DetailDto detailDto) {
        detailRepository.save(detailMapper.toEntity(detailDto));
        return detailRepository.existsByNameAndSeriesNumber(detailDto.getName(), detailDto.getSeriesNumber());
    }

    @Override
    @Transactional
    public void delete(Long id) {
        detailRepository.deleteById(id);
    }

    private static ArrayList<DetailDto> getLimitFeed(int firstCarOnPage, int lastCarOnPage, List<DetailDto> detailDtos) {
        ArrayList<DetailDto> limitCarsList = new ArrayList<>();
        if (firstCarOnPage > detailDtos.size() - 1) {
            throw new VeryBigNumberOfPageException();
        }
        for (int i = firstCarOnPage; i < lastCarOnPage; i++) {
            if (i <= detailDtos.size()) {
                limitCarsList.add(detailDtos.get(i));
            }
        }
        return limitCarsList;
    }
}
