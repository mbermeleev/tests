package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.enums.Currency;
import org.example.exceptions.DriverNotFoundException;
import org.example.models.Driver;
import org.example.parameters.ParametersBalance;
import org.example.repositories.DriverRepository;
import org.example.services.BalanceAndBirthdayService;
import org.example.services.CrudDriverService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
@RequiredArgsConstructor
public class BalanceAndBirthdayServiceImpl implements BalanceAndBirthdayService {

    private final DriverRepository driverRepository;
    private final CrudDriverService crudDriverService;

    @Override
    @Transactional
    public Double getBalance(Long driversId, String currencyType) {
        Driver driver = driverRepository.findById(driversId).orElseThrow(DriverNotFoundException::new);
        Double balance = driver.getDriversBalance();
        if (currencyType.equals(Currency.GREEN.name())) {
            return balance;
        } else if (currencyType.equals(Currency.RED.name())) {
            return balance * 2.5;
        } else {
            return balance / 0.6;
        }
    }

    @Override
    @Transactional
    public Double changeBalance(Long driversId, Double amountOfChange, ParametersBalance parametersBalance) {
        Driver driver = driverRepository.findById(driversId).orElseThrow(DriverNotFoundException::new);
        double newBalance;
        if (parametersBalance.getIsIncreaseBalance()) {
            newBalance = driver.getDriversBalance() + amountOfChange;
        } else {
            newBalance = driver.getDriversBalance() - amountOfChange;
        }
        driver.setDriversBalance(newBalance);
        driverRepository.save(driver);
        return newBalance;
    }

    @Override
    @Transactional
    @Scheduled(cron = "@daily")
    public void sendCongratulation() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formattedString = date.format(formatter);

        var drivers = driverRepository.findAll();
        for (Driver driver : drivers) {
            if (driver.getBirthdayDate().equals(formattedString)) {
                log.info("Поздравление с днём рождения водителя "
                        + driver.getFirstName() + " "
                        + driver.getLastName() + " "
                        + driver.getPatronymic());
            }
        }
    }
}
