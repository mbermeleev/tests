package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.CarDto;
import org.example.dto.DetailDto;
import org.example.dto.DriverDto;
import org.example.exceptions.CarNotFoundException;
import org.example.exceptions.DriverNotFoundException;
import org.example.exceptions.IncorrectNumberOfPageException;
import org.example.exceptions.VeryBigNumberOfPageException;
import org.example.mappers.CarMapper;
import org.example.mappers.DetailMapper;
import org.example.mappers.DriverMapper;
import org.example.models.Car;
import org.example.models.Detail;
import org.example.models.Driver;
import org.example.repositories.CarRepository;
import org.example.repositories.DetailRepository;
import org.example.repositories.DriverRepository;
import org.example.services.CarsAndDriversService;
import org.example.services.CrudCarService;
import org.example.services.CrudDriverService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarsAndDriversServiceImpl implements CarsAndDriversService, CrudCarService, CrudDriverService {

    private final CarRepository carRepository;
    private final DetailRepository detailRepository;
    private final DriverRepository driverRepository;
    private final CarMapper carMapper;
    private final DetailMapper detailMapper;
    private final DriverMapper driverMapper;

    @Override
    @Transactional
    public String changeDriverFromCar(String VIN, Long driverId) {
        var car = carRepository.findById(VIN).orElseThrow(CarNotFoundException::new);
        var driver = driverRepository.findById(driverId).orElseThrow(DriverNotFoundException::new);

        car.setDriver(driver);
        driver.setCar(car);

        carRepository.save(car);
        driverRepository.save(driver);

        return driver.getFirstName() + " " + driver.getLastName() + " " + driver.getPatronymic();
    }

    @Override
    @Transactional
    public ArrayList<DetailDto> addNewDetailToCar(String vin, DetailDto detailDto) {
        detailRepository.save(detailMapper.toEntity(detailDto));
        var car = carRepository.findById(vin).get();
        var details = car.getDetails();
        details.add(detailMapper.toEntity(detailDto));
        car.setDetails(details);
        carRepository.save(car);

        return (ArrayList<DetailDto>) car.getDetails()
                .stream()
                .map(detailMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ArrayList<DetailDto> changeDetailInCar(Long detailIdWhichNeedToChange, String vin, DetailDto newDetail) {
        detailRepository.save(detailMapper.toEntity(newDetail));
        var car = carRepository.findById(vin).get();
        var details = car.getDetails();
        ArrayList<Detail> newDetailsList = new ArrayList<>();
        for (Detail detail : details) {
            if (!(detail.getId().equals(detailIdWhichNeedToChange))) {
                newDetailsList.add(detail);
            }
        }
        newDetailsList.add(detailMapper.toEntity(newDetail));
        car.setDetails(newDetailsList);
        carRepository.save(car);

        detailRepository.deleteCarFromDetail(detailIdWhichNeedToChange);

        return (ArrayList<DetailDto>) car.getDetails()
                .stream()
                .map(detailMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ArrayList<DetailDto> deleteDetailFromCar(Long detailIdToDelete, String vin) {
        var car = carRepository.findById(vin).get();
        var details = car.getDetails();
        ArrayList<Detail> newDetailsList = new ArrayList<>();
        for (Detail detail : details) {
            if (!(detail.getId().equals(detailIdToDelete))) {
                newDetailsList.add(detail);
            }
        }
        car.setDetails(newDetailsList);

        carRepository.save(car);
        detailRepository.deleteCarFromDetail(detailIdToDelete);

        return (ArrayList<DetailDto>) car.getDetails()
                .stream()
                .map(detailMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public DriverDto getOneDriver(Long id) {
        return driverMapper.toDto(driverRepository.findById(id).orElseThrow(DriverNotFoundException::new));
    }

    @Override
    @Transactional
    public ArrayList<DriverDto> getAllDrivers(int page) {
        ArrayList<DriverDto> drivers = (ArrayList<DriverDto>) driverRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Driver::getId))
                .map(driverMapper::toDto).toList();
        if (page < 1) {
            throw new IncorrectNumberOfPageException();
        }
        final int carsCountOnPage = 30;
        int firstCarOnPage = page > 1 ? ((carsCountOnPage * (page - 1)) - 1) : 0;
        int lastCarOnPage = (carsCountOnPage * page) - 1;

        return CarsAndDriversServiceImpl.getLimitFeedDrivers(firstCarOnPage, lastCarOnPage, drivers);
    }

    @Override
    @Transactional
    public Boolean updateDriver(Long id, DriverDto driverDto) {
        var driver = driverRepository.findById(id).orElseThrow(DriverNotFoundException::new);
        driver.setCar(driverDto.getCar());
        driver.setFirstName(driver.getFirstName());
        driver.setLastName(driver.getLastName());
        driverRepository.save(driver);
        return true;
    }

    @Override
    @Transactional
    public Boolean saveDriver(DriverDto driverDto) {
        driverRepository.save(driverMapper.toEntity(driverDto));
        return driverRepository.existsByPassportSeriesNumber(driverDto.getPassportSeriesAndNumber());
    }

    @Override
    @Transactional
    public void deleteDriver(Long id) {
        driverRepository.deleteById(id);
    }

    private static ArrayList<DriverDto> getLimitFeedDrivers(int firstCarOnPage, int lastCarOnPage, List<DriverDto> driverDtos) {
        ArrayList<DriverDto> limitDriversList = new ArrayList<>();
        if (firstCarOnPage > driverDtos.size() - 1) {
            throw new VeryBigNumberOfPageException();
        }
        for (int i = firstCarOnPage; i < lastCarOnPage; i++) {
            if (i <= driverDtos.size()) {
                limitDriversList.add(driverDtos.get(i));
            }
        }
        return limitDriversList;
    }

    @Override
    @Transactional
    public CarDto getOneCar(String VIN) {
        return carMapper.toDto(carRepository.findById(VIN).orElseThrow(CarNotFoundException::new));
    }

    @Override
    @Transactional
    public ArrayList<CarDto> getAllCars(int page) {
        ArrayList<CarDto> cars = (ArrayList<CarDto>) carRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Car::getVIN))
                .map(carMapper::toDto).toList();
        if (page < 1) {
            throw new IncorrectNumberOfPageException();
        }
        final int carsCountOnPage = 30;
        int firstCarOnPage = page > 1 ? ((carsCountOnPage * (page - 1)) - 1) : 0;
        int lastCarOnPage = (carsCountOnPage * page) - 1;

        return getLimitFeedCars(firstCarOnPage, lastCarOnPage, cars);
    }

    @Override
    @Transactional
    public Boolean updateCar(CarDto carDto) {
        var car = carRepository.findById(carDto.getVIN())
                .orElseThrow(CarNotFoundException::new);
        car.setStateNumber(carDto.getStateNumber());
        car.setManufacturer(carDto.getManufacturer());
        carRepository.save(car);
        return true;
    }

    @Override
    @Transactional
    public Boolean saveCar(CarDto carDto) {
        carRepository.save(carMapper.toEntity(carDto));
        return carRepository.existsById(carDto.getVIN());
    }

    @Override
    @Transactional
    public void deleteCar(String VIN) {
        carRepository.deleteById(VIN);
    }

    private static ArrayList<CarDto> getLimitFeedCars(int firstCarOnPage, int lastCarOnPage, List<CarDto> carDtos) {
        ArrayList<CarDto> limitCarsList = new ArrayList<>();
        if (firstCarOnPage > carDtos.size() - 1) {
            throw new VeryBigNumberOfPageException();
        }
        for (int i = firstCarOnPage; i < lastCarOnPage; i++) {
            if (i <= carDtos.size()) {
                limitCarsList.add(carDtos.get(i));
            }
        }
        return limitCarsList;
    }
}
