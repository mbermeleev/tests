package org.example.models;

import jakarta.persistence.*;
import lombok.*;
import org.example.enums.Currency;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String licenseCategory;
    private String birthdayDate;
    private Long passportSeriesNumber;
    private Integer drivingExperience;
    private Double driversBalance;
    private Currency currencyType = Currency.GREEN;

    @OneToOne
    private Car car;
}
