package org.example.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@ToString(exclude = "driver")
public class Car {

    @Id
    @NotNull
    private String VIN;
    private String stateNumber;
    private String manufacturer;
    private String carBrand;
    private String yearOfManufacture;

    @OneToOne
    private Driver driver;

    @OneToMany
    private ArrayList<Detail> details;
}
