package org.example.repositories;

import org.example.models.Detail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DetailRepository extends JpaRepository<Detail, Long> {
    Boolean existsByNameAndSeriesNumber(String name, String seriesNumber);

    @Modifying
    @Query("update #{#entityName} d set d.car = null where d.id = ?1")
    void deleteCarFromDetail(Long detailIdWhichNeedToChange);
}
