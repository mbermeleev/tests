package org.example.repositories;

import org.example.models.Driver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<Driver, Long> {
    Boolean existsByPassportSeriesNumber(Long passportSeriesAndNumber);
}
