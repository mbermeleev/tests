package org.example.dto;

import lombok.*;
import org.example.models.Detail;
import org.example.models.Driver;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarDto {

    private String VIN;
    private String stateNumber;
    private String manufacturer;
    private String carBrand;
    private String yearOfManufacture;
    private Driver driver;
    private ArrayList<Detail> details;
}
