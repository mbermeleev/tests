package org.example.dto;

import lombok.*;
import org.example.models.Car;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailDto {

    private Long id;
    private String name;
    private String seriesNumber;
    private Car car;
}
