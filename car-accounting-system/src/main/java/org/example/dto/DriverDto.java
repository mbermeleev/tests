package org.example.dto;

import lombok.*;
import org.example.enums.Currency;
import org.example.models.Car;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DriverDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String licenseCategory;
    private String birthdayDate;
    private Long passportSeriesAndNumber;
    private Integer drivingExperience;
    private Double driversBalance;
    private Currency currencyType = Currency.GREEN;
    private Car car;
}
