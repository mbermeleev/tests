package org.example.repository;

import jakarta.transaction.Transactional;
import org.example.repositories.DriverRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("CommentService is working for methods")
@SpringBootTest
class DriverRepositoryTest {

    @Autowired
    private DriverRepository driverRepository;

    @Test
    void existsByPassportSeriesAndNumber() {
    }

    @DisplayName(("existsDriverByPassportSeriesAndNumber() is working"))
    @Nested
    @Transactional
    class ForExistsDriverByPassportSeriesAndNumber {
        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(longs = {1L, 2L})
        void on_passport_series_and_number_which_have_in_repo(Long passportSeriesAndNumber) {
            assertTrue(driverRepository.existsByPassportSeriesNumber(passportSeriesAndNumber));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(longs = {100L, 700L})
        void on_passport_series_and_number_which_not_have_in_repo(Long passportSeriesAndNumber) {
            assertFalse(driverRepository.existsByPassportSeriesNumber(passportSeriesAndNumber));
        }
    }
}