package org.example.controllers;

import org.example.exceptions.CarNotFoundException;
import org.example.services.CarsAndDriversService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("CarsAndDriversController is working")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CarsAndDriversControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarsAndDriversService carsAndDriversService;

    @BeforeEach
    public void setUp() {
        when(carsAndDriversService.changeDriverFromCar("1234567890", 1L))
                .thenReturn("Иванов Иван Иванович");
        when(carsAndDriversService.changeDriverFromCar("123", 1L)).thenThrow(CarNotFoundException.class);
    }

    @Test
    public void return_string() throws Exception {
        mockMvc.perform(post("/cars_and_drivers/changeDriverToCar/1234567890/1"))
                .andDo(print());
    }

    @Test
    void return_exception() throws Exception {
        mockMvc.perform(post("/cars_and_drivers/changeDriverToCar/123/1"))
                .andDo(print());
    }
}