drop table if exists jobs;

create table jobs(ID integer primary key generated always as identity, depCode varchar(20) not null, depJob varchar(100) not null, description varchar(255), unique (depCode, depJob));