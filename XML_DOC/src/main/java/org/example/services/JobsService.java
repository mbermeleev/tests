package org.example.services;

import org.springframework.stereotype.Service;

@Service
public interface JobsService {
    boolean getAllEntriesFromDBToXml(String fileName);
    boolean synchroDataBaseAndXml(String fileNameOnWhichSynchro, String fileNameForSynchro);
}
