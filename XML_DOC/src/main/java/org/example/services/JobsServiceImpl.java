package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.models.Job;
import org.example.repositories.JobsRepository;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 29.10.2023
 *
 * @author Bermeleyev Marat
 */
@Service(value = "jobService")
@RequiredArgsConstructor
@PropertySource(value = "classpath:application.properties")
public class JobsServiceImpl implements JobsService {

    private final JobsRepository jobsRepository;
    private static final String ROOT_DIRECTORY = "root.directory";
    private final File root = new File(ROOT_DIRECTORY);
    private HashMap<String, Job> newJobs = new HashMap<>();
    private HashMap<String, Job> jobsForSynchro = new HashMap<>();
    private ArrayList<Job> jobsForAddOrUpdate = new ArrayList<>();
    private ArrayList<Job> jobsForDelete = new ArrayList<>();

    @Override
    public boolean getAllEntriesFromDBToXml(String fileName) {



        return false;
    }

    @Override
    @Transactional
    public boolean synchroDataBaseAndXml(String fileNameOnWhichSynchro, String fileNameForSynchro) {

        try {
            newJobs = getMapFromXml(fileNameOnWhichSynchro);
            if (newJobs.isEmpty()) return false;
            jobsForSynchro = getMapFromXml(fileNameForSynchro);

            saveJobsForAddOrUpdate();
            saveJobsForDelete();

            jobsRepository.saveOrUpdate(jobsForAddOrUpdate);
            jobsRepository.deleteBySynchro(jobsForDelete);
            return true;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveJobsForAddOrUpdate() {
        for (Map.Entry<String, Job> entry : newJobs.entrySet()) {
            Job jobFromJobsForSynchro = jobsForSynchro.get(entry.getKey());
            if (jobFromJobsForSynchro == null)
                jobsForAddOrUpdate.add(entry.getValue());
            else if (jobFromJobsForSynchro != null && !jobFromJobsForSynchro.equals(entry.getValue()))
                jobsForAddOrUpdate.add(jobFromJobsForSynchro);
        }
    }

    private void saveJobsForDelete() {
        for (Map.Entry<String, Job> entry : jobsForSynchro.entrySet()) {
            Job jobFromNewJobs = newJobs.get(entry.getKey());
            if (jobFromNewJobs == null)
                jobsForDelete.add(entry.getValue());
        }
    }

    private HashMap<String, Job> getMapFromXml(String fileNameOnWhichSynchro) throws ParserConfigurationException, SAXException, IOException {
        HashMap<String, Job> jobs = new HashMap<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();

        var filePath = getFilePathByFileName(fileNameOnWhichSynchro, root.listFiles());
        if (filePath.isBlank()) return new HashMap<>();

        var file = new File(filePath);
        Document document = documentBuilder.parse(file);

        Element element = document.getDocumentElement();
        NodeList nodeList = element.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i) instanceof Element) {
                NodeList nodes = nodeList.item(i).getChildNodes();
                for (int j = 0; j < nodes.getLength(); j++) {
                    Node childElement = nodes.item(j);
                    if (childElement instanceof Element) {
                        var jobFromXml = Job.builder()
                                .depCode(((Element) childElement).getAttribute("DepCode"))
                                .depJob(((Element) childElement).getAttribute("DepJob"))
                                .description(((Element) childElement).getAttribute("Description"))
                                .build();
                        jobs.put(jobFromXml.getKeyForMap(), jobFromXml);
                    }
                }
            }
        }
        return jobs;
    }

    private String getFilePathByFileName(String fileName, File[] files) {
        String filePath = "";
        for (File file : files) {
            if (file.isDirectory())
                return getFilePathByFileName(fileName, file.listFiles());
            if (file.getName().equals(fileName))
                return file.getAbsolutePath();
        }
        return filePath;
    }
}
