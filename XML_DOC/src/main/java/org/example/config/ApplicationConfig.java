package org.example.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@RequiredArgsConstructor
@PropertySource(value = "classpath:application.properties")
@ComponentScan(basePackages = "org.example")
@Configuration
public class ApplicationConfig {
    private final Environment environment;

    private DataSource dataSource;

    @Bean
    public DataSource dataSource(HikariConfig config) {
        this.dataSource = new HikariDataSource(config);
        return this.dataSource;
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setDriverClassName(environment.getProperty("db.driver-class-name"));
        config.setMaximumPoolSize(Integer.parseInt(environment.getProperty("db.hikari.max-pool-size")));
        return config;
    }

    @Bean
    public void SqlScripts() {
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScripts(
                new ClassPathResource("schema.sql"),
                new ClassPathResource("data.sql"));
        populator.setSeparator("@@");
        populator.execute(this.dataSource);
    }

    @Bean
    public DataSource getDataSource() {
        return dataSource;
    }
}
