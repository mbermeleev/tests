package org.example.models;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Job {

    private int ID;
    private String depCode;
    private String depJob;
    private String description;

    public String getKeyForMap() {
        return depCode + ", " + depJob;
    }
}
