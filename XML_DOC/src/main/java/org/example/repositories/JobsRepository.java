package org.example.repositories;

import org.example.models.Job;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface JobsRepository {

    void saveOrUpdate(ArrayList<Job> jobsForAddOrUpdate);
    void deleteBySynchro(ArrayList<Job> jobsForDelete);
    ArrayList<Job> getAllFromDB();
}
