package org.example.repositories;

import org.example.models.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.function.Function;

@Repository
public class JobsRepositoryJdbcImpl implements JobsRepository{

    //language=SQL
    private static final String SQL_INSERT_OR_UPDATE = """
            INSERT INTO jobs (depcode, depjob, description)
            VALUES (:depCode, :depJob, :description)
            ON CONFLICT (depcode, depjob)
            DO UPDATE SET depcode = excluded.depcode, depjob = excluded.depjob, description = excluded.description""";

    //language=SQL
    private static final String SQL_DELETE_BY_NATURAL_KEY = """
            DELETE FROM jobs
            WHERE depcode = :depCode AND depjob = :depJob""";

    //language=SQL
    private static final String SQL_SELECT_ALL = """
            SELECT depcode, depjob, description FROM jobs""";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final DataSource dataSource;

    @Autowired
    public JobsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final Function<ResultSet, Job> jobMapper = resultSet -> {
        try {
            String depCode = resultSet.getString("depcode");
            String depJob = resultSet.getString("depjob");
            String description = resultSet.getString("description");
            return Job.builder()
                    .depCode(depCode)
                    .depJob(depJob)
                    .description(description)
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void saveOrUpdate(ArrayList<Job> jobsForAddOrUpdate) {
        for (Job job : jobsForAddOrUpdate) {
            Map<String, Object> params = new HashMap<>();
            params.put("depCode", job.getDepCode());
            params.put("depJob", job.getDepJob());
            params.put("description", job.getDescription());

            SqlParameterSource parameterSource = new MapSqlParameterSource(params);
            namedParameterJdbcTemplate.update(SQL_INSERT_OR_UPDATE, parameterSource);
        }
    }

    @Override
    public void deleteBySynchro(ArrayList<Job> jobsForDelete) {
        for (Job job : jobsForDelete) {
            Map<String, Object> params = new HashMap<>();
            params.put("depCode", job.getDepCode());
            params.put("depJob", job.getDepJob());

            SqlParameterSource parameterSource = new MapSqlParameterSource(params);
            namedParameterJdbcTemplate.update(SQL_DELETE_BY_NATURAL_KEY, parameterSource);
        }
    }

    @Override
    public ArrayList<Job> getAllFromDB() {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {

            ArrayList<Job> jobs = new ArrayList<>();

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    jobs.add(jobMapper.apply(resultSet));
                }
                return jobs;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
