package org.example;

import org.example.config.ApplicationConfig;
import org.example.enums.Commands;
import org.example.repositories.JobsRepository;
import org.example.services.JobsService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

/**
 * 29.10.2023
 * XML_DOC_TEST_PROJECT
 *
 * @author Bermeleyev Marat
 * @version v1.0
 */
public class App {
    public static void main( String[] args ) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        JobsService jobsService = context.getBean(JobsService.class);

        Scanner scanner = new Scanner(System.in);
        String toDo = scanner.nextLine();

        while (!toDo.equals("0")) {

            String[] strings = toDo.split(" ");

            switch (Commands.valueOf(strings[0])) {
                case GET: {
                    jobsService.getAllEntriesFromDBToXml(strings[1]);
                    break;
                }
                case ADD: {

                    break;
                }
                case DELETE: {

                    break;
                }
                case SYNCHRO: {
                    jobsService.synchroDataBaseAndXml(strings[1], strings[2]);
                    break;
                }
            }
            toDo = scanner.nextLine();
        }
    }
}
