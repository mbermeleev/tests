package org.example.config.security.authorities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.example.enums.Role;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Authorities {

    @SuppressWarnings("InstantiationOfUtilityClass")
    @Getter
    private static final Authorities instance = new Authorities();

    public static final String ROLE_ADMIN = Role.ADMIN.name();
    public static final String ROLE_USER = Role.USER.name();
}
