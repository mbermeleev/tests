package org.example.config.security.details;

import lombok.RequiredArgsConstructor;
import org.example.models.Person;
import org.example.repositories.PersonsRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.example.utils.AnotherUtils.USER_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final PersonsRepository personsRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<Person> person = personsRepository.findByLogin(login);
        if (person.isEmpty())
            throw new UsernameNotFoundException(USER_NOT_FOUND);
        return new CustomUserDetails(person.get());
    }
}
