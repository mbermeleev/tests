package org.example.mappers;

import org.example.dto.MessageDto;
import org.example.models.Message;
import org.mapstruct.Mapper;

@Mapper
public interface MessageMapper {
    MessageDto toDto(Message message);
    Message toEntity(MessageDto messageDto);
}
