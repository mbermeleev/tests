package org.example.mappers;

import org.example.dto.ApplicationDto;
import org.example.models.Application;
import org.mapstruct.Mapper;

@Mapper
public interface ApplicationMapper {
    ApplicationDto toDto(Application application);
    Application toEntity(ApplicationDto applicationDto);
}
