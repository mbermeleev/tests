package org.example.mappers;

import org.example.dto.PersonDto;
import org.example.models.Person;
import org.mapstruct.Mapper;

@Mapper
public interface PersonMapper {
    PersonDto toDto(Person person);
    Person toEntity(PersonDto personDto);
}
