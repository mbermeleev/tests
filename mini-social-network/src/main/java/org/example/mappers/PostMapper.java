package org.example.mappers;

import org.example.dto.PostDto;
import org.example.models.Post;
import org.mapstruct.Mapper;

@Mapper
public interface PostMapper {
    PostDto toDto(Post post);
    Post toEntity(PostDto postDto);
}
