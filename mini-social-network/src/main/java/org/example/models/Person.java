package org.example.models;

import jakarta.persistence.*;
import lombok.*;
import org.example.enums.Role;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private Integer age;
    private Role role;

    @OneToMany
    @JoinTable(name = "subscriptions",joinColumns = {@JoinColumn(name = "person_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "subscriptions_id", referencedColumnName = "id")})
    private List<Person> subscriptions;

    @OneToMany
    @JoinTable(name = "subscribers",joinColumns = {@JoinColumn(name = "person_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "subscribers_id", referencedColumnName = "id")})
    private List<Person> subscribers;

    @OneToMany
    @JoinTable(name = "friends",joinColumns = {@JoinColumn(name = "person_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "friends_id", referencedColumnName = "id")})
    private List<Person> friends;

    @OneToMany
    @ToString.Exclude
    private List<Post> posts;

    @OneToMany
    @ToString.Exclude
    private List<Message> chat;

    @OneToMany
    @ToString.Exclude
    private List<Application> applications;

}
