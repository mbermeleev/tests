package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.AuthDto;
import org.example.servicesImpl.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Tag(name="Контроллер аутентификации",
        description="Аутентифицируем пользователя и получаем токен для него")
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @Operation(
            summary = "Аутентификация пользователя",
            description = "Позволяет аутентифицировать пользователя и получить его токен"
    )
    @GetMapping("/signIn")
    public ResponseEntity<String> getAuthentication(@RequestBody @Parameter(description = "Сущность аутентификации")
                                                        AuthDto authDto) {
        return ResponseEntity.ok().body(authenticationService.getAuthentication(authDto));
    }
}


