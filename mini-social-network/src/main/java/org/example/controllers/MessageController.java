package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.MessageDto;
import org.example.services.MessageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/message")
@RequiredArgsConstructor
@Tag(name = "Контроллер сообщений",
        description = "Позволяет совершать операции связанные с сообщениями")
public class MessageController {

    private final MessageService messageService;

    @Operation(summary = "Отправка сообщения другу",
            description = "Позволяет отправить сообщение другу")
    @PostMapping("/sendMessage/{personIdFrom}/{personIdTo}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<HttpStatus> sendMessage(@PathVariable @Parameter(description = "Id отправляющего пользователя")
                                                      Long personIdFrom,
                                                  @PathVariable @Parameter(description = "Id получающего пользователя")
                                                  Long personIdTo,
                                                  @RequestBody @Parameter(description = "Сообщение")
                                                      MessageDto messageDto) {
        messageDto.setFromPerson(personIdFrom);
        messageDto.setToPerson(personIdTo);
        messageService.sendMessage(messageDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @Operation(summary = "Просмотр чата",
            description = "Позволяет посмотреть переписку с пользователем"
    )
    @GetMapping("{personIdWhoGet}/getChatWith/{personIdWith}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<List<MessageDto>> getChat(@PathVariable @Parameter(description = "Id пользователя, запрашивающего чат")
                                                        Long personIdWhoGet,
                                                 @PathVariable @Parameter(description = "Id пользователя, с которым нужна переписка")
                                                 Long personIdWith) {
        return ResponseEntity.ok().body(messageService.getChat(personIdWhoGet, personIdWith));
    }
}
