package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.PostDto;
import org.example.services.PostService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.example.utils.AnotherUtils.*;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
@Tag(name = "Контроллер постов",
    description = "Позволяет совершать операции связанные с постами")
public class PostController {

    private final PostService postService;

    @Operation(summary = "Удаление поста",
            description = "Позволяет пользователю удалить пост")
    @DeleteMapping("/deletePost/{postId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<String> deletePost(@PathVariable @Parameter(description = "Id поста, который удаляется")
                                                 Long postId) {
        postService.deletePost(postId);
        return ResponseEntity.ok(POST_HAS_DELETED);
    }

    @Operation(summary = "Просмотр поста",
            description = "Позволяет посмотреть пост")
    @GetMapping("/getPost/{postId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<PostDto> getPost(@PathVariable @Parameter(description = "Id поста, который просматриваем")
                                               Long postId) {
        return ResponseEntity.ok().body(postService.getPostById(postId));
    }

    @Operation(summary = "Изменение поста",
            description = "Позволяет изменить пост и добавить к нему файл(фото)")
    @PatchMapping("/updatePost/{postId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<PostDto> updatePost(@PathVariable @Parameter(description = "Id поста, который изменяем")
                                                  Long postId,
                                              @RequestPart @Parameter(description = "Файл(фото), который добавляем")
                                                    MultipartFile file,
                                              @RequestPart @Parameter(description = "Обновленный пост")
                                                  PostDto postDto) {
        return ResponseEntity.ok().body(postService.updatePost(postId, file, postDto));
    }

    @Operation(summary = "Просмотр ленты постов",
            description = "Позволяет получить ленту всех постов пользователей, на которых подписан запрашивающий")
    @GetMapping("/{personWhoGetId}/getFeed?param=page")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<List<PostDto>> getFeed(@PathVariable @Parameter(description = "Id пользователя, который смотрит ленту постов")
                                                     Long personWhoGetId,
                                                 @RequestParam @Parameter(description = "Номер страницы, минимальный номер страницы: '1'")
                                                    Integer page) {
        return ResponseEntity.ok().body(postService.getFeed(personWhoGetId, page));
    }
}
