package org.example.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Hidden
@Tag(name = "Контроллер обработки исключений",
        description = "Позволяет обработать собственные исключения")
public class ExceptionAPIHandlerController {

    @Operation(
            summary = "Обработка ошибки поиска пользователя в базе",
            description = "Позволяет получить сообщение о неверно введенных данных(логин и пароль)"
    )
    @ExceptionHandler(PersonNotFoundException.class)
    public ResponseEntity<PersonNotFoundException> personNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new PersonNotFoundException());
    }

    @Operation(
            summary = "Обработка ошибки регистрации",
            description = "Позволяет обработать ошибку регистрации"
    )
    @ExceptionHandler(RegistrationFaildException.class)
    public ResponseEntity<RegistrationFaildException> registrationFaildException() {
        return ResponseEntity.status(600).body(new RegistrationFaildException());
    }

    @Operation(
            summary = "Обработка неверно указанной страницы",
            description = "Позволяет обработать ошибку в указании номера страницы"
    )
    @ExceptionHandler(IncorrectNumberOfPageException.class)
    public ResponseEntity<IncorrectNumberOfPageException> incorrectNumberOfPageException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new IncorrectNumberOfPageException());
    }

    @Operation(
            summary = "Обработка неверно указанной страницы",
            description = "Позволяет обработать ошибку в указании номера страницы"
    )
    @ExceptionHandler(VeryBigNumberOfPageException.class)
    public ResponseEntity<VeryBigNumberOfPageException> veryBigNumberOfPageException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new VeryBigNumberOfPageException());
    }

    @Operation(
            summary = "Обработка одинакового логина при регистрации",
            description = "Позволяет обработать ошибку одинакового логина для регистрирующегося пользователя," +
                    "если пользователь с таким логином уже зарегистрирован."
    )
    @ExceptionHandler(PersonWithThisLoginAlreadyRegistered.class)
    public ResponseEntity<PersonWithThisLoginAlreadyRegistered> personWithThisLoginAlreadyRegistered() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new PersonWithThisLoginAlreadyRegistered());
    }

    @Operation(
            summary = "Обработка ошибки поиска пользователя",
            description = "Позволяет обработать ошибку при поиске пользователя по id, которого нет в базе."
    )
    @ExceptionHandler(PersonWithThisIdNotFoundException.class)
    public ResponseEntity<PersonWithThisIdNotFoundException> personWithThisIdNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new PersonWithThisIdNotFoundException());
    }
}
