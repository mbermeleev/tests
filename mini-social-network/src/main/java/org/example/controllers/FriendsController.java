package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.ApplicationDto;
import org.example.services.FriendsService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/friends")
@RequiredArgsConstructor
@Tag(name = "Контроллер списка друзей и подписок",
        description = "Позволяет совершать операции связанные со списком друзей и подписок")
public class FriendsController {

    private final FriendsService friendsService;

    @Operation(
            summary = "Заявка на добавление в друзья",
            description = "Позволяет отправить заявку пользователю на добавление в друзья, " +
                    "также добавляет в подписчики пользователя, отправляющего заявку пользователю, " +
                    "которому отправляют заявку. Также добавляет пользователю, отправляющему заявку, " +
                    "подписку на другого пользователя."
    )
    @PostMapping("/{personIdFrom}/sendFriendRequest/{personIdTo}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<String> sendFriendRequest(@PathVariable @Parameter(description = "Id отправляющего заявку")
                                                        Long personIdFrom,
                                                    @PathVariable @Parameter(description = "Id получающего заявку")
                                                        Long personIdTo,
                                                    @RequestBody @Parameter(description = "Заявка")
                                                        ApplicationDto applicationDto) {
        return ResponseEntity.ok().body(
                friendsService.sendFriendRequest(personIdFrom, personIdTo, applicationDto));
    }

    @Operation(
            summary = "Одобрение заявки на добавление в друзья",
            description = "Позволяет добавить в друзья другого пользователя, отправившего заявку, " +
                    "также добавить его в подписки, также стать подписчиком на этого пользователя"
    )
    @PostMapping("/addToFriends/{applicationId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<String> addToFriends(@PathVariable @Parameter(description = "Id заявки")
                                                   Long applicationId) {
        return ResponseEntity.ok().body(friendsService.addToFriends(applicationId));
    }

    @Operation(
            summary = "Отклонение заявки на добавление в друзья",
            description = "Позволяет отклонить заявку в друзья, пользователь отправивший заявку, " +
                    "остается подписчиком"
    )
    @PostMapping("/denyApplicationToFriend/{applicationId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<String> denyApplicationToFriend(@PathVariable @Parameter(description = "Id заявки")
                                                              Long applicationId) {
        return ResponseEntity.ok().body(friendsService.denyApplicationToFriend(applicationId));
    }

    @Operation(
            summary = "Операция удаления из друзей",
            description = "Позволяет удалить пользователя из друзей и также отписаться от него, " +
                    "также удаляет из друзей у другого пользователя, того, который совершает эту операцию, " +
                    "также оставляет в подписчиках пользователя, которого удалили из друзей"
    )
    @PostMapping("/deleteFromFriends/{personWhoDeleteId}/{personForDeleteId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<String > deleteFromFriends(@PathVariable @Parameter(description = "Id удаляющего пользователя")
                                                         Long personWhoDeleteId,
                                                     @PathVariable @Parameter(description = "Id пользователя, которого удаляют")
                                                     Long personForDeleteId) {
        return ResponseEntity.ok().body(friendsService.deleteFromFriends(personWhoDeleteId, personForDeleteId));
    }
}
