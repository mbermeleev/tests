package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.PersonDto;
import org.example.dto.PostDto;
import org.example.services.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
@Tag(name = "Пользовательский контроллер",
        description = "Позволяет пользователю совершать определенные операции")
public class PersonController {

    private final PersonService personService;

    @Operation(summary = "Создание нового поста",
            description = "Позволяет пользователю создать новый пост")
    @PostMapping("/{personId}/createPost")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<PostDto> createPostFromPerson(@PathVariable @Parameter(description = "Id пользователя создающего пост")
                                                            Long personId,
                                                        @RequestPart @Parameter(description = "Сам пост")
                                                            PostDto postDto,
                                                        @RequestPart @Parameter(description = "Файл(фото) прикрепленный к посту")
                                                            MultipartFile file) throws IOException {
        return ResponseEntity.ok().body(personService.createNewPostFromPerson(personId, postDto, file));
    }

    @Operation(summary = "Просмотр всех постов пользователя",
            description = "Позволяет посмотреть все посты определенного пользователя")
    @GetMapping("/getPostByPerson/{personId}")
    @SecurityRequirement(name = "JWT")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_USER)")
    public ResponseEntity<List<PostDto>> getAllPostsFromPerson(@PathVariable @Parameter(description = "Id пользователя, чью посты просматриваем")
                                                                   Long personId) {
        return ResponseEntity.ok().body(personService.getAllPostsFromPerson(personId));
    }
}
