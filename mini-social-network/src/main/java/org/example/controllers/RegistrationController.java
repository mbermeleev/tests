package org.example.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.example.dto.SignUpDto;
import org.example.servicesImpl.RegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reg")
@RequiredArgsConstructor
@Tag(name="Контроллер Регистрации",
        description="Регистрируем пользователя и добавляем его в базу данных")
public class RegistrationController {

    private final RegistrationService registrationService;

    @Operation(
            summary = "Регистрация пользователя",
            description = "Позволяет зарегистрировать пользователя"
    )
    @PostMapping("/signUp")
    public ResponseEntity<String> signUp(@RequestBody @Parameter(description = "Сущность регистрации")
                                             SignUpDto signUpDto) {
        return ResponseEntity.ok().body(registrationService.signUp(signUpDto));
    }
}
