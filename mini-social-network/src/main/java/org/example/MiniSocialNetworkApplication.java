package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniSocialNetworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniSocialNetworkApplication.class, args);
    }

}
