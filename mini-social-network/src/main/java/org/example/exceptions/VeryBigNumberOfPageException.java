package org.example.exceptions;

public class VeryBigNumberOfPageException extends RuntimeException {
    public VeryBigNumberOfPageException(){
        super("Был указан слишком большой номер страницы, укажи меньший номер!");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
