package org.example.exceptions;

public class PersonNotFoundException extends RuntimeException{

    public PersonNotFoundException() {
        super("Пользователь с таким логином/паролем не найден, введите верные данные!");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
