package org.example.exceptions;

public class PersonWithThisLoginAlreadyRegistered extends RuntimeException {
    public PersonWithThisLoginAlreadyRegistered() {
        super("Данный логин уже занят другим пользователем. Придумайте другой логин.");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
