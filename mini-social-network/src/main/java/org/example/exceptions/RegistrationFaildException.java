package org.example.exceptions;

public class RegistrationFaildException extends RuntimeException {
    public RegistrationFaildException() {
        super("Регистрация прошла неудачно");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
