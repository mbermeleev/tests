package org.example.exceptions;

public class PersonWithThisIdNotFoundException extends RuntimeException{

    public PersonWithThisIdNotFoundException() {
        super("Аккаунт с таким id не найден!");
    }
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
