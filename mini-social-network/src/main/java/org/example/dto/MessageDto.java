package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Сущность сообщения")
public class MessageDto {

    @Schema(description = "Id сообщения")
    private Long id;
    @Schema(description = "Id пользователя, который отправляет сообщение")
    private Long fromPerson;
    @Schema(description = "Id пользователя - получателя")
    private Long toPerson;
    @Schema(description = "Текст сообщения")
    private String text;
}
