package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.example.models.Person;

@Data
@Builder
@Schema(description = "Сущность заявки")
public class ApplicationDto {

    @Schema(description = "Id заявки")
    private Long id;
    @Schema(description = "Id пользователя, отправляющего заявку")
    private Long fromPerson;
    @Schema(description = "Текст заявки")
    private String text;

    @Schema(description = "Сущность пользователя(не нужно заполнять при отправке заявки)")
    private Person person;
}
