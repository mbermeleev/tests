package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.example.enums.Role;
import org.example.models.Message;
import org.example.models.Post;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Сущность пользователя")
public class PersonDto {

    @Schema(description = "Id пользователя")
    private Long id;
    @Schema(description = "Имя пользователя")
    private String firstName;
    @Schema(description = "Фамилия пользователя")
    private String lastName;
    @Schema(description = "Логин пользователя")
    private String login;
    @Schema(description = "Пароль пользователя")
    private String password;
    @Schema(description = "Email пользователя")
    private String email;
    @Schema(description = "Возраст пользователя")
    private Integer age;
    @Schema(description = "Список всех подписок пользователя")
    private List<PersonDto> subscriptions;
    @Schema(description = "Список всех подписчиков пользователя")
    private List<PersonDto> subscribers;
    @Schema(description = "Список всех друзей пользователя")
    private List<PersonDto> friends;
    @Schema(description = "Список всех постов созданных пользователем")
    private List<Post> posts;
    @Schema(description = "Список всех сообщений пользователя и пользователю")
    private List<Message> chat;
    @Schema(description = "")
    private Role role;
}
