package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.example.models.Person;
import org.example.models.Photo;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Сущность поста")
public class PostDto {

    @Schema(description = "Id поста")
    private Long id;
    @Schema(description = "Заголовок поста")
    private String heading;
    @Schema(description = "Текст поста")
    private String text;
    @Schema(description = "Список файлов(фото), прикрепленных к посту")
    private List<Photo> photos;
    @Schema(description = "Сущность пользователя")
    private Person person;
    @Schema(description = "Время создания поста")
    private LocalDateTime createdAt;
}
