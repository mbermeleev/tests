package org.example.services;

import org.example.dto.ApplicationDto;
import org.example.dto.MessageDto;
import org.example.dto.PersonDto;
import org.example.dto.PostDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface PersonService {
    void save(PersonDto personDto);

    PostDto createNewPostFromPerson(Long personId, PostDto postDto, MultipartFile file) throws IOException;

    List<PostDto> getAllPostsFromPerson(Long personId);

    void addApplicationToPerson(Long personIdTo, ApplicationDto applicationDto);

    void addSubscriberToPerson(Long personIdTo, Long personIdFrom);

    void addSubscriptionToPerson(Long personIdFrom, Long personIdTo);

    void addNewFriend(Long applicationId);

    void deleteFromFriends(Long personWhoDeleteId, Long personForDeleteId);

    void sendMessage(MessageDto messageDto);

    PersonDto getPerson(Long personId);
}
