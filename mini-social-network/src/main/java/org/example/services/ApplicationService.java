package org.example.services;

import org.springframework.stereotype.Service;

@Service
public interface ApplicationService {
    void delete(Long applicationId);
}
