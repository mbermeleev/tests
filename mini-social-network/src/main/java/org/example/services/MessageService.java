package org.example.services;

import org.example.dto.MessageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {
    void sendMessage(MessageDto messageDto);

    List<MessageDto> getChat(Long personIdWhoGet, Long personIdWith);
}
