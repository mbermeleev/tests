package org.example.services;

import org.example.dto.PostDto;
import org.example.models.Post;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface PostService {
    void deletePost(Long postId);

    PostDto getPostById(Long postId);

    PostDto updatePost(Long postId, MultipartFile file, PostDto postDto);

    List<Post> getAllPostsFromDB();

    List<PostDto> getFeed(Long personWhoGetId, Integer page);
}
