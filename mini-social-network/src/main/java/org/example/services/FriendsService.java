package org.example.services;

import org.example.dto.ApplicationDto;
import org.springframework.stereotype.Service;

@Service
public interface FriendsService {
    String sendFriendRequest(Long personIdFrom, Long personIdTo, ApplicationDto applicationDto);

    String addToFriends(Long applicationId);

    String denyApplicationToFriend(Long applicationId);

    String deleteFromFriends(Long personWhoDeleteId, Long personForDeleteId);
}
