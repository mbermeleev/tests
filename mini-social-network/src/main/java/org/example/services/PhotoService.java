package org.example.services;

import org.example.models.Photo;
import org.springframework.stereotype.Service;

@Service
public interface PhotoService {
    void savePhoto(Photo photo);
}
