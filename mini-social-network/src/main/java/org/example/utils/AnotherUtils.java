package org.example.utils;

public class AnotherUtils {
    public static final String DIRECTORY_PATH = "src/main/resources/photoStorage";
    public static final String USER_NOT_FOUND = "Пользователь с таким логином/паролем не найден!";
    public static final String BEARER = "Bearer ";
    public static final String AUTHORIZATION = "Authorization";
    public static final String INVALID_JWT_TOKEN_IN_BEARER = "Invalid JWT token in Bearer header";
    public static final String SECRET = "Secret key";
    public static final String POST_HAS_DELETED = "Пост был удален";
    public static final String APPLICATION_HAS_SENT = "Заявка была отправлена пользователю";
    public static final String PERSON_HAS_ADDED = "Пользователь был добавлен в друзья";
    public static final String APPLICATION_HAS_DENIED = "Заявка была отклонена";
    public static final String FRIEND_DELETED = "Пользователь был удален из списка друзей";
}
