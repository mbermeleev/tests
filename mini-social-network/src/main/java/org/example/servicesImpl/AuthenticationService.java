package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.AuthDto;
import org.example.exceptions.PersonNotFoundException;
import org.springframework.security.authentication.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    public String getAuthentication(AuthDto authDTO) {
        try {
            UsernamePasswordAuthenticationToken authInputToken =
                    new UsernamePasswordAuthenticationToken(
                            authDTO.getLogin(),
                            authDTO.getPassword());
            authenticationManager.authenticate(authInputToken);
            return jwtService.generateToken(authDTO);
        } catch (BadCredentialsException e) {
            throw new PersonNotFoundException();
        }
    }
}
