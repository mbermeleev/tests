package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.MessageDto;
import org.example.mappers.MessageMapper;
import org.example.models.Message;
import org.example.services.MessageService;
import org.example.services.PersonService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final PersonService personService;
    private final MessageMapper messageMapper;

    @Override
    public void sendMessage(MessageDto messageDto) {
        personService.sendMessage(messageDto);
    }

    @Override
    public List<MessageDto> getChat(Long personIdWhoGet, Long personIdWith) {
        var person = personService.getPerson(personIdWhoGet);

        var allMessagesFromPersonWhoGet = person.getChat();
        List<Message> chat = new ArrayList<>();
        for (Message message : allMessagesFromPersonWhoGet) {
            if (message.getFromPerson().equals(personIdWith) ||
                    message.getToPerson().equals(personIdWith)) {

                chat.add(message);
            }
        }

        return chat.stream().map(messageMapper::toDto).collect(Collectors.toList());
    }
}
