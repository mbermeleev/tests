package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.models.Photo;
import org.example.repositories.PhotosRepository;
import org.example.services.PhotoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PhotoServiceImpl implements PhotoService {

    private final PhotosRepository photosRepository;

    @Override
    public void savePhoto(Photo photo) {
        photosRepository.save(photo);
    }
}
