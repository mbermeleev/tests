package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.repositories.ApplicationRepository;
import org.example.services.ApplicationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;

    @Override
    public void delete(Long applicationId) {
        applicationRepository.deleteById(applicationId);
    }
}
