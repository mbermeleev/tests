package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.ApplicationDto;
import org.example.services.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.example.utils.AnotherUtils.*;

@Service
@Transactional
@RequiredArgsConstructor
public class FriendsServiceImpl implements FriendsService {

    private final PersonService personService;
    private final ApplicationService applicationService;

    @Override
    public String sendFriendRequest(Long personIdFrom, Long personIdTo, ApplicationDto applicationDto) {

        applicationDto.setFromPerson(personIdFrom);

        personService.addApplicationToPerson(personIdTo, applicationDto);
        personService.addSubscriberToPerson(personIdTo, personIdFrom);
        personService.addSubscriptionToPerson(personIdFrom, personIdTo);

        return APPLICATION_HAS_SENT;
    }

    @Override
    public String addToFriends(Long applicationId) {

        personService.addNewFriend(applicationId);

        return PERSON_HAS_ADDED;
    }

    @Override
    public String denyApplicationToFriend(Long applicationId) {
        applicationService.delete(applicationId);
        return APPLICATION_HAS_DENIED;
    }

    @Override
    public String deleteFromFriends(Long personWhoDeleteId, Long personForDeleteId) {

        personService.deleteFromFriends(personWhoDeleteId, personForDeleteId);

        return FRIEND_DELETED;
    }
}
