package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.*;
import org.example.enums.Role;
import org.example.exceptions.*;
import org.example.mappers.*;
import org.example.repositories.*;
import org.example.models.*;
import org.example.services.PersonService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.stream.Collectors;
import java.util.*;

@Service
@Transactional
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonsRepository personsRepository;
    private final PersonMapper personMapper;
    private final PasswordEncoder passwordEncoder;
    private final PostMapper postMapper;
    private final ApplicationMapper applicationMapper;
    private final ApplicationRepository applicationRepository;
    private final MessageMapper messageMapper;

    @Override
    public PersonDto getPerson(Long personId) {
        return personMapper.toDto(personsRepository.findById(personId).orElseThrow(PersonWithThisIdNotFoundException::new));
    }

    @Override
    public void save(PersonDto personDto) {
        if (personsRepository.existsPersonByLogin(personDto.getLogin())) {
            throw new PersonWithThisLoginAlreadyRegistered();
        }
        personsRepository.save(Person.builder()
                        .login(personDto.getLogin())
                        .password(passwordEncoder.encode(personDto.getPassword()))
                        .email(personDto.getEmail())
                        .role(Role.USER)
                        .build());
    }

    @Override
    public PostDto createNewPostFromPerson(Long personId, PostDto postDto, MultipartFile file) throws IOException {
        ArrayList<Photo> photos = new ArrayList<>();
        if (!file.isEmpty()) {
            Photo photo = Photo.builder()
                    .fileName(file.getOriginalFilename())
                    .size(file.getSize())
                    .type(file.getContentType())
                    .bytes(file.getBytes())
                    .build();
            photos.add(photo);
            postDto.setPhotos(photos);
            postDto.setCreatedAt(LocalDateTime.now());
        }
        personsRepository.saveNewPostToPerson(personId, postMapper.toEntity(postDto));

        return postDto;
    }

    @Override
    public List<PostDto> getAllPostsFromPerson(Long personId) {
        return personsRepository.findById(personId).get().getPosts()
                .stream()
                .map(postMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void addApplicationToPerson(Long personIdTo, ApplicationDto applicationDto) {
        var person = personsRepository.findById(personIdTo).get();
        applicationDto.setPerson(person);
        applicationRepository.save(applicationMapper.toEntity(applicationDto));
        var applications = person.getApplications();
        applications.add(applicationMapper.toEntity(applicationDto));
        person.setApplications(applications);
        personsRepository.save(person);
    }

    @Override
    public void addSubscriberToPerson(Long personIdTo, Long personIdFrom) {
        var person = personsRepository.findById(personIdTo).get();
        var subscriber = personsRepository.findById(personIdFrom).get();
        var subscribers = person.getSubscribers();
        subscribers.add(subscriber);
        person.setSubscribers(subscribers);
        personsRepository.save(person);
    }

    @Override
    public void addSubscriptionToPerson(Long personIdFrom, Long personIdTo) {
        var person = personsRepository.findById(personIdFrom).get();
        var subscriber = personsRepository.findById(personIdFrom).get();
        var subscriptions = subscriber.getSubscriptions();
        subscriptions.add(person);
        subscriber.setSubscriptions(subscriptions);
        personsRepository.save(subscriber);
    }

    @Override
    public void addNewFriend(Long applicationId) {
        var application = applicationRepository.findById(applicationId).get();

        var personForAddToFriends = personsRepository.findById(application.getFromPerson()).get();
        var personWhoAddNewFriend = personsRepository.findById(application.getPerson().getId()).get();

        var friendsFromPersonForAdd = personForAddToFriends.getFriends();
        friendsFromPersonForAdd.add(personWhoAddNewFriend);
        personForAddToFriends.setFriends(friendsFromPersonForAdd);

        var friendsFromPersonWhoAdd = personWhoAddNewFriend.getFriends();
        friendsFromPersonWhoAdd.add(personForAddToFriends);
        personWhoAddNewFriend.setFriends(friendsFromPersonWhoAdd);

        personsRepository.save(personForAddToFriends);
        personsRepository.save(personWhoAddNewFriend);

        addSubscriptionToPerson(personWhoAddNewFriend.getId(), personForAddToFriends.getId());
    }

    @Override
    public void deleteFromFriends(Long personWhoDeleteId, Long personForDeleteId) {
        var personWhoDelete = personsRepository.findById(personWhoDeleteId).get();
        var deletedPerson = personsRepository.findById(personForDeleteId).get();

        deleteFriendFromPerson(deletedPerson, personWhoDelete);
        deleteFriendFromDeletedPerson(personWhoDelete, deletedPerson);
    }

    private void deleteFriendFromDeletedPerson(Person personWhoDelete, Person deletedPerson) {
        var friendsFromDeletedPerson = deletedPerson.getFriends();
        var subscribersFromDeletedPerson = deletedPerson.getSubscribers();

        if (friendsFromDeletedPerson.remove(personWhoDelete)) {
            if (subscribersFromDeletedPerson.remove(personWhoDelete)) {
                deletedPerson.setFriends(friendsFromDeletedPerson);
                deletedPerson.setSubscribers(subscribersFromDeletedPerson);
                personsRepository.save(deletedPerson);
            }
        }
    }

    private void deleteFriendFromPerson(Person personForDelete, Person personWhoDelete) {
        var newFriendsList = personWhoDelete.getFriends();
        var newSubscriptions = personWhoDelete.getSubscriptions();
        if (newFriendsList.remove(personForDelete)) {
            if (newSubscriptions.remove((personForDelete))) {
                personWhoDelete.setFriends(newFriendsList);
                personWhoDelete.setSubscriptions(newSubscriptions);
                personsRepository.save(personWhoDelete);
            }
        }
    }

    @Override
    public void sendMessage(MessageDto messageDto) {
        var personWhoSend = personsRepository.findById(messageDto.getFromPerson()).get();
        var personWhomSent = personsRepository.findById(messageDto.getToPerson()).get();

        saveMessageToPerson(messageDto, personWhoSend);
        saveMessageToPerson(messageDto, personWhomSent);
    }

    private void saveMessageToPerson(MessageDto messageDto, Person person) {
        var personWhomSentChat = person.getChat();
        personWhomSentChat.add(messageMapper.toEntity(messageDto));
        person.setChat(personWhomSentChat);
        personsRepository.save(person);
    }

}
