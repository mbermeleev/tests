package org.example.servicesImpl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import org.example.dto.AuthDto;
import org.example.exceptions.PersonNotFoundException;
import org.example.repositories.PersonsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import static org.example.utils.SecurityUtils.*;
import static org.example.utils.AnotherUtils.*;

@Service
@Transactional
@RequiredArgsConstructor
public class JwtService {
    private final PersonsRepository personsRepository;

    public String generateToken(AuthDto authDto) {
        var person = personsRepository.findByLogin(authDto.getLogin()).orElseThrow(PersonNotFoundException::new);
        Instant expirationDate = Instant.from(ZonedDateTime.now().plusMinutes(20).toInstant());
        return JWT.create()
                .withClaim(LOGIN_CLAIM, person.getLogin())
                .withClaim(ID_CLAIM, person.getId())
                .withClaim(EMAIL_CLAIM, person.getEmail())
                .withClaim(FIRST_NAME_CLAIM, person.getFirstName())
                .withClaim(LAST_NAME_CLAIM, person.getLastName())
                .withClaim(ROLE_CLAIM, String.valueOf(person.getRole()))
                .withIssuedAt(new Date())
                .withExpiresAt(expirationDate)
                .sign(Algorithm.HMAC256(SECRET));
    }
}
