package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.SignUpDto;
import org.example.enums.Role;
import org.example.exceptions.PersonWithThisLoginAlreadyRegistered;
import org.example.models.Person;
import org.example.repositories.PersonsRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class RegistrationService {

    private final PersonsRepository personsRepository;
    private final PasswordEncoder passwordEncoder;
    public String signUp(SignUpDto signUpDto) {

        if (personsRepository.existsPersonByLogin(signUpDto.getLogin())) {
            throw new PersonWithThisLoginAlreadyRegistered();
        }

        Person person = Person.builder()
                .login(signUpDto.getLogin())
                .password(passwordEncoder.encode(signUpDto.getPassword()))
                .email(signUpDto.getEmail())
                .role(Role.USER)
                .build();

        personsRepository.save(person);

        return "Пользователь " + signUpDto.getLogin() + " был успешно зарегистрирован!";
    }
}
