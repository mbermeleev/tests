package org.example.servicesImpl;

import lombok.RequiredArgsConstructor;
import org.example.dto.PersonDto;
import org.example.dto.PostDto;
import org.example.exceptions.IncorrectNumberOfPageException;
import org.example.exceptions.VeryBigNumberOfPageException;
import org.example.mappers.PostMapper;
import org.example.models.Person;
import org.example.models.Photo;
import org.example.models.Post;
import org.example.repositories.PostsRepository;
import org.example.services.PersonService;
import org.example.services.PostService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostsRepository postsRepository;
    private final PostMapper postMapper;
    private final PersonService personService;

    @Override
    public void deletePost(Long postId) {
        postsRepository.deleteById(postId);
    }

    @Override
    public PostDto getPostById(Long postId) {
        return postMapper.toDto(postsRepository.findById(postId).get());
    }

    @Override
    public PostDto updatePost(Long postId, MultipartFile file, PostDto postDto) {
        var post = postsRepository.findById(postId).get();
        ArrayList<Photo> photos = (ArrayList<Photo>) post.getPhotos();
        Photo photo;
        if (!file.isEmpty()) {
            try {
                photo = Photo.builder()
                        .fileName(file.getOriginalFilename())
                        .size(file.getSize())
                        .type(file.getContentType())
                        .bytes(file.getBytes())
                        .build();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            photos.add(photo);
        }
        post.setText(postDto.getText());
        post.setHeading(postDto.getHeading());
        post.setPhotos(photos);

        postsRepository.save(post);
        return postMapper.toDto(post);
    }

    @Override
    public List<Post> getAllPostsFromDB() {
        return postsRepository.findAll();
    }

    @Override
    public List<PostDto> getFeed(Long personWhoGetId, Integer page) {
        if (page < 1) {
            throw new IncorrectNumberOfPageException();
        }
        final int postsCountOnPage = 30;
        int firstPostOnPage = page > 1 ? ((postsCountOnPage * (page - 1)) - 1) : 0;
        int lastPostOnPage = (postsCountOnPage * page) - 1;
        var personWhoGet = personService.getPerson(personWhoGetId);
        List<Post> allPosts = getAllPostsFromDB();
        List<PersonDto> subscriptions = personWhoGet.getSubscriptions();

        List<Post> feed = getFullFeed(allPosts, subscriptions);;

        List<Post> limitFeed = getLimitFeed(firstPostOnPage, lastPostOnPage, feed);

        return limitFeed.stream().map(postMapper::toDto).collect(Collectors.toList());
    }

    private static List<Post> getLimitFeed(int firstPostOnPage, int lastPostOnPage, List<Post> feed) {
        List<Post> limitFeed = new ArrayList<>();
        if (firstPostOnPage > feed.size() - 1) {
            throw new VeryBigNumberOfPageException();
        }
        for (int i = firstPostOnPage; i < lastPostOnPage; i++) {
            if (i <= feed.size()) {
                limitFeed.add(feed.get(i));
            }
        }
        return limitFeed;
    }

    private List<Post> getFullFeed(List<Post> allPosts, List<PersonDto> subscriptions) {
        List<Post> feed = new ArrayList<>();
        for (Post post : allPosts) {
            if (isPresentForSubscriptions(post.getPerson(), subscriptions)) {
                feed.add(post);
            }
        }
        return feed;
    }

    private boolean isPresentForSubscriptions(Person person, List<PersonDto> subscriptions) {
        for (PersonDto subscription : subscriptions) {
            if (person.getId().equals(subscription.getId())) {
                return true;
            }
        }
        return false;
    }
}
