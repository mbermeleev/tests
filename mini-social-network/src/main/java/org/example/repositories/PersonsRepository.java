package org.example.repositories;

import org.example.models.Person;
import org.example.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PersonsRepository extends JpaRepository<Person, Long> {
    Optional<Person> findByLogin(String login);

    @Modifying
    @Query("update #{#entityName} p set p.posts = ?2 where p.id = ?1")
    void saveNewPostToPerson(Long personId, Post post);

    boolean existsPersonByLogin(String login);
}
