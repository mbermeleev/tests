package org.example.repositories;

import org.example.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PostsRepository extends JpaRepository<Post, Long> {
    Optional<Post> findByText(String text);
}
