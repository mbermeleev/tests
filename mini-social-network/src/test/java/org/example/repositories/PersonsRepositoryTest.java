package org.example.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("PersonRepository working when")
class PersonsRepositoryTest {

    @Autowired
    private PersonsRepository personsRepository;

    @DisplayName("method getPerson(Long id) work when")
    @Nested
    class ForMethodGetPerson {

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(strings = {"login", "admin"})
        void on_id_which_have_in_repo(String login) {
            assertTrue(personsRepository.existsPersonByLogin(login));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(strings = {"test", "user"})
        void on_id_which_have_not_in_repo(String login) {
            assertFalse(personsRepository.existsPersonByLogin(login));
        }
    }
}