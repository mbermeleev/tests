package org.example.controllers;

import org.example.models.Person;
import org.example.repositories.PersonsRepository;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("RegistrationController work when")
@TestPropertySource("/application-test.yml")
class RegistrationControllerTest {

    private final MockMvc mockMvc;

    @Autowired
    public RegistrationControllerTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @DisplayName("method signUp(SignUpDto signUpDto) work")
    @Nested
    class ForSignUpTests{

        @Test
        public void return_ok_status_for_registration() throws Exception {
            mockMvc.perform(post("/reg/signUp")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{ \n" +
                            "\"login\" : \"user\",\n" +
                            "\"password\" : \"password\",\n" +
                            "\"email\" : \"user@mail.ru\"\n" +
                            "}")).andExpect(status().isOk());
        }

        @Test
        public void fail_registration() throws Exception {
            mockMvc.perform(post("/reg/signUp")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{ \n" +
                            "\"login\" : \"admin\",\n" +
                            "\"password\" : \"admin\",\n" +
                            "\"email\" : \"admin@mail.ru\"\n" +
                            "}"))
                    .andExpect(status().isBadRequest());
        }
    }
}