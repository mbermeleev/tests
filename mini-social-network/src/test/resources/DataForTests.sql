INSERT INTO person (login, email, password, role)
VALUES ('login', 'email@mail.ru', '$2a$10$M5qu1.sfL9U5Gf5GScN0dO9BAcpprPNV7VgnKFYFLMMMfZwcYLBPG', 0);

INSERT INTO person (login, email, password, role)
VALUES ('admin', 'admin@mail.ru', '$2a$12$FIyQiSEjfW2kekqdYttyAeOTAI7uvFmzbndNH9504kupCGkiHo5y6', 1);